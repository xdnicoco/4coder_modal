#if !defined(_MODAL_THEME_CPP)
#define _MODAL_THEME_CPP

inline ARGB_Color modal_invert_color(ARGB_Color color){
    Vec4_f32 inverted = unpack_color(color);
    f32 r = 1.f - inverted.r;
    f32 g = 1.f - inverted.g;
    f32 b = 1.f - inverted.b;
    f32 xmax = Max(Max(r, g), b);
    f32 xmin = Min(Min(r, g), b);
    f32 c = xmax - xmin;
    f32 l = (xmax + xmin)/2;
    f32 t = 2*l-1;
    if(t < 0) t = -t;
    
    f32 h = 0, s = 0;
    
    if(l > 0) {
        if(xmax == r) {
            h = 60.f*(((g-b)/c)+0);
        } else if(xmax == g) {
            h = 60.f*(((b-r)/c)+2);
        } else {
            h = 60.f*(((r-g)/c)+4);
        }
        
        s = c/(1-t);
    }
    
    h += 180.f;
    if(h >= 360.f) h -= 360.f;
    
    c = s*(1-t);
    t = (f32)fmod(h/60.f, 2.f) - 1.f;
    if(t < 0.f) t = -t;
    f32 x = c*(1.f-t);
    f32 m = l - c/2.f;
    
    if     (h < 60.f)  { r = c + m; g = x + m; b = 0 + m; }
    else if(h < 120.f) { r = x + m; g = c + m; b = 0 + m; }
    else if(h < 180.f) { r = 0 + m; g = c + m; b = x + m; }
    else if(h < 240.f) { r = 0 + m; g = x + m; b = c + m; }
    else if(h < 300.f) { r = x + m; g = 0 + m; b = c + m; }
    else if(h < 360.f) { r = c + m; g = 0 + m; b = x + m; }
    
    inverted.r = Max(r, 0);
    inverted.g = Max(g, 0);
    inverted.b = Max(b, 0);
    
    return pack_color(inverted);
}

inline void modal_invert_theme_colors() {
    Color_Table *table = &active_color_table;
    
    for(i32 color_index = 0; color_index < table->count; ++color_index) {
        Color_Array *array = &table->arrays[color_index];
        for(i32 i = 0; i < array->count; ++i) {
            array->vals[i] = modal_invert_color(array->vals[i]);
        }
    }
}

#define MODAL_THEME_SET_ALPHA(tag, pos, alpha) (active_color_table.arrays[defcolor_##tag].vals[pos] = fcolor_resolve(fcolor_change_alpha(fcolor_argb(active_color_table.arrays[defcolor_##tag].vals[pos]), alpha)))
#define MODAL_THEME_SET_COLOR(tag, pos, color) (active_color_table.arrays[defcolor_##tag].vals[pos] = color)
#define MODAL_THEME_COPY_COLOR(tag1, pos1, tag2, pos2) (active_color_table.arrays[defcolor_##tag1].vals[pos1] = active_color_table.arrays[defcolor_##tag2].vals[pos2])


CUSTOM_COMMAND_SIG(modal_set_base_theme)
CUSTOM_DOC("Sets the base colors for the modal layer.") {
    Color_Table *table = &active_color_table;
    
    Arena *arena = &global_theme_arena;
    linalloc_clear(arena);
    *table = make_color_table(app, arena);
    
#define MOD_TABLE_MAKE_COLOR(tag, ...) table->arrays[defcolor_##tag] = make_colors(arena, __VA_ARGS__)
#define MOD_TABLE_COPY_COLOR(tag, c1) table->arrays[defcolor_##tag] = make_colors(arena, table->arrays[defcolor_##c1].vals[0])
#define MOD_TABLE_COPY_COLORS(tag, c1, c2) table->arrays[defcolor_##tag] = make_colors(arena, table->arrays[defcolor_##c1].vals[0], table->arrays[defcolor_##c2].vals[0])
    
    MOD_TABLE_MAKE_COLOR(back,          0xff050f15);
    MOD_TABLE_MAKE_COLOR(margin,        0xff030439);
    MOD_TABLE_MAKE_COLOR(margin_hover,  0xff020f1f);
    MOD_TABLE_MAKE_COLOR(margin_active, 0xff051449);
    
    MOD_TABLE_COPY_COLORS(list_item,        margin,        back);
    MOD_TABLE_COPY_COLORS(list_item_hover,  margin_hover,  margin);
    MOD_TABLE_COPY_COLORS(list_item_active, margin_active, margin_active);
    
    MOD_TABLE_MAKE_COLOR(cursor,       0xff104489, 0xffff99ff);
    MOD_TABLE_MAKE_COLOR(mark,         0xff0d6695);
    MOD_TABLE_MAKE_COLOR(text_default, 0xffdadaca);
    
    MOD_TABLE_MAKE_COLOR(at_cursor,    0x90eeeeff);
    MOD_TABLE_COPY_COLOR(at_highlight, text_default);
    MOD_TABLE_MAKE_COLOR(highlight,             0x5528a287);
    MOD_TABLE_MAKE_COLOR(highlight_cursor_line, 0x2f104489);
    MOD_TABLE_MAKE_COLOR(comment,               0xff7e9e7e);
    MOD_TABLE_MAKE_COLOR(comment_pop,           0xff5fff5f, 0xffff5f5f, 0xffffe041); // <-- for NOTE/TODO/IMPORTANT etc.
    MOD_TABLE_MAKE_COLOR(keyword,               0xfff5c59b);
    
    MOD_TABLE_MAKE_COLOR(str_constant,   0xffcd9494);
    MOD_TABLE_COPY_COLOR(char_constant,  str_constant);
    MOD_TABLE_MAKE_COLOR(int_constant,   0xffcab080);
    MOD_TABLE_COPY_COLOR(float_constant, int_constant);
    MOD_TABLE_COPY_COLOR(bool_constant,  int_constant);
    
    MOD_TABLE_COPY_COLOR(include,           str_constant);
    MOD_TABLE_MAKE_COLOR(preproc,           0xff6be0e3);
    MOD_TABLE_MAKE_COLOR(special_character, 0xffff0000);
    MOD_TABLE_MAKE_COLOR(ghost_character,   0xff5b4d3c);
    
    MOD_TABLE_MAKE_COLOR(paste,           0xffe6644d);
    MOD_TABLE_MAKE_COLOR(undo,            0xff2dffab);
    MOD_TABLE_MAKE_COLOR(highlight_junk,  0xff3a0000);
    MOD_TABLE_MAKE_COLOR(highlight_white, 0x55003a3a);
    
    MOD_TABLE_COPY_COLOR(bar, margin);
    MOD_TABLE_MAKE_COLOR(base, 0xffaaaaaa);
    MOD_TABLE_MAKE_COLOR(pop1, 0xff5fff5f);
    MOD_TABLE_MAKE_COLOR(pop2, 0xffff5f5f);
    
    MOD_TABLE_MAKE_COLOR(back_cycle, 0x0aa00000, 0x0a00a000, 0x0a0000a0, 0x08a0a000);
    MOD_TABLE_MAKE_COLOR(text_cycle, 0xffa00000, 0xff00a000, 0xff0030b0, 0xffa0a000);
    
    MOD_TABLE_MAKE_COLOR(line_numbers_back, 0xff020a10);
    MOD_TABLE_MAKE_COLOR(line_numbers_text, 0xff336655);
    
#undef MOD_TABLE_MAKE_COLOR
#undef MOD_TABLE_COPY_COLOR
#undef MOD_TABLE_COPY_COLORS
}

#endif // _MODAL_THEME_CPP