#if !defined(_MODAL_BINDINGS_HUD_CPP)
#define _MODAL_BINDINGS_HUD_CPP

//
// The following file implement a hud for displaying the available key bindings
// from the active key mapping. To use it inside your own layer,
// you can add something like this to the function you use to render your buffer
// (usually, default_render_buffer in 4coder_default_hooks):
//
// if(is_active_view && global_show_bindings_hud) {
//     mod_draw_bindings_hud(app, view_id, buffer, face_id);
// }
//
// Also, see :BindingHudModalLayerSpecific: for things that are specific to my layer
// that you'd might like to change.
//

#include "modal_bindings_hud.h"

//
// The actual command that handles the input for the bindings HUD
//
CUSTOM_COMMAND_SIG(mod_show_bindings_hud)
CUSTOM_DOC("Shows a hud describing the available bindings.") {
    Mod_Bindings_Hud_State *state = &global_bindings_hud_state;
    state->show_hud = true;
    for(;;) {
        User_Input input = get_next_input(app, EventPropertyGroup_AnyUserInput, 0);
        if(input.abort) break;
        if(input.event.kind == InputEventKind_MouseWheel) {
            Mouse_State mouse = get_mouse_state(app);
            
            // :BindingHudModalLayerSpecific: Hardcoded for my trackpad, which is jank
            state->hud_base_y += mouse.wheel/8.f;
        } else if(input.event.kind == InputEventKind_MouseButtonRelease) {
            Command_Metadata *meta = state->hot_command_meta;
            if(meta) {
                if(has_modifier(&input.event.mouse.modifiers, KeyCode_Shift)) {
                    View_ID view = get_active_view(app, Access_ReadWrite);
                    
                    Name_Line_Column_Location location = {};
                    location.file = SCu8(meta->source_name, meta->source_name_len);
                    location.line = meta->line_number;
                    
                    jump_to_location(app, view, location);
                    
                    // Delete this line or create a stub if you don't use the rest of my layer
                    modal_set_mode_normal(app); // :BindingHudModalLayerSpecific:
                } else {
                    meta->proc(app);
                }
            }
            
            // :BindingHudModalLayerSpecific: I want the hud to be kept open
            // for certain modes in my layer, you should probably break here
            // for most use cases.
            if(modal_mode_should_close_hud(global_active_mapid)) break;
        } else if((input.event.kind == InputEventKind_KeyRelease) ||
                  (input.event.kind == InputEventKind_KeyStroke)) {
            // If the user pressed a valid key, proceed to actualy run the command.
            // TODO(NJ): Figure out how to make it actually take the input hook
            // Copypasta from default_view_input_handler in 4coder_default_hooks.cpp
            // :BindingHudModalLayerSpecific:
            
            if (implicit_map_function == 0){
                implicit_map_function = default_implicit_map;
            }
            Implicit_Map_Result map_result = implicit_map_function(app, 0, 0, &input.event);
            if(map_result.command) {
                map_result.command(app);
                // :BindingHudModalLayerSpecific:
                if(modal_mode_should_close_hud(global_active_mapid)) break;
            }
        }
    }
    
    state->show_hud = false;
}

// Colors for the hud, I calculate them from the active them but you might want
// to do something else. :BindingHudModalLayerSpecific:
function void
mod_fill_binding_hud_colors(Mod_Bindings_Hud_Colors *colors) {
    colors->back = fcolor_id(defcolor_back);
    colors->back = fcolor_blend(fcolor_id(defcolor_cursor), .7f, colors->back);
    colors->back = fcolor_change_alpha(colors->back, .95f);
    
    colors->title1 = fcolor_id(defcolor_pop1);
    colors->title2 = fcolor_id(defcolor_pop2);
    colors->base   = fcolor_id(defcolor_base);
    colors->keys   = fcolor_blend(colors->base, .4f, fcolor_id(defcolor_pop1));
    colors->mods   = fcolor_blend(colors->base, .4f, fcolor_id(defcolor_pop2));
    colors->margin = fcolor_id(defcolor_margin);
    colors->cursor = fcolor_id(defcolor_cursor);
    
    colors->separator = fcolor_change_alpha(colors->keys, .7f);
    
    colors->description_back = fcolor_blend(colors->cursor, .5f, colors->back);
}

//
// Helpers
//

Vec2_f32 mod_wrap_text_to_width(Application_Links *app, Arena *arena, List_String_Const_u8 *wrapped,
                                String_Const_u8 text, Face_ID face, f32 paragraph_width, f32 line_advance) {
    f32 height = 0.f;
    f32 width  = 0.f;
    
    Range_i64 line_range = {};
    do {
        // NOTE(FS): I use multi-line documentations sometimes, see the comment in
        // modes/snippet.cpp to see how to fix 4coder_metadata_generator.cpp to not
        // wrongly report documentation string size when using escaped characters.
        
        // Break text to lines:
        line_range.end = string_find_first(text, line_range.start + 1, '\n');
        String_Const_u8 line = string_substring(text, line_range);
        
        Range_i64 word_range = {};
        Range_i64 wrap_range = {};
        while(wrap_range.end < (i64)line.size) {
            height += line_advance;
            
            // Break lines to wrapped lines:
            word_range.start = wrap_range.end;
            wrap_range.start = wrap_range.end;
            
            f32 wrap_width = 0;
            do {
                // Break wrapped lines to words:
                word_range.end = string_find_first(line, word_range.start + 1, ' ');
                
                String_Const_u8 word = string_substring(line, word_range);
                
                Fancy_String fancy_word = {};
                fancy_word.value = word;
                
                f32 word_width = get_fancy_string_width(app, face, &fancy_word);
                
                if((wrap_width + word_width < paragraph_width) || (wrap_range.start == wrap_range.end)) {
                    wrap_width += word_width;
                    wrap_range.end = word_range.end + 1; // +1 to eat the space
                    word_range.start = word_range.end;
                } else {
                    break;
                }
            } while(wrap_range.end < (i64)line.size);
            
            wrap_range.end = Min(wrap_range.end, (i64)line.size);
            String_Const_u8 wrapped_line = string_substring(line, wrap_range);
            string_list_push(arena, wrapped, wrapped_line);
            
            width = Max(width, wrap_width);
        }
        
        line_range.start = line_range.end;
    } while(line_range.start < (i64)text.size);
    
    return V2f32(width, height);
}

// modified copypasta from 4coder_command_map.cpp
function String_Const_u8
mod_command_trigger_stringize_mods(Arena *arena, Input_Modifier_Set *modifiers){
    String_Const_u8 result = {};
    if (modifiers->count > 0){
        Key_Code *mods = modifiers->mods;
        result = push_string_const_u8(arena, 2*modifiers->count);
        for (i32 i = 0; i < modifiers->count; i += 1){
            char *mod_name = ArraySafe(key_code_name, mods[i]);
            result.str[2*i + 0] = mod_name[0];
            result.str[2*i + 1] = ' ';
        }
    }
    
    return result;
}

function String_Const_u8
mod_command_trigger_stringize_mods(Arena *arena, Command_Trigger *trigger) {
    return mod_command_trigger_stringize_mods(arena, &trigger->mods);
}

// copypasta from 4coder_command_map.cpp
function String_Const_u8
mod_command_trigger_stringize(Arena *arena, Command_Trigger *trigger){
    String_Const_u8 result = {};
    switch (trigger->kind){
        case InputEventKind_KeyStroke:
        case InputEventKind_KeyRelease: {
            String_Const_u8 prefix = {};
            if(trigger->kind == InputEventKind_KeyRelease) prefix = string_u8_litexpr("Release ");
            
            String_Const_u8 key_name = SCu8(ArraySafe(key_code_name, trigger->sub_code));
            if(key_name.size == 1) {
                result = push_u8_stringf(arena, "%.*s%c",   string_expand(prefix), character_to_lower(key_name.str[0]));
            } else {
                result = push_u8_stringf(arena, "%.*s%.*s", string_expand(prefix), string_expand(key_name));
            }
        } break;
        
        case InputEventKind_TextInsert:         { result = push_string_copy(arena, string_u8_litexpr("Text Insert"));                                  } break;
        case InputEventKind_MouseButton:        { result = push_u8_stringf (arena, "Mouse %s", ArraySafe(mouse_code_name, trigger->sub_code));         } break;
        case InputEventKind_MouseButtonRelease: { result = push_u8_stringf (arena, "Release Mouse %s", ArraySafe(mouse_code_name, trigger->sub_code)); } break;
        case InputEventKind_MouseWheel:         { result = push_string_copy(arena, string_u8_litexpr("Mouse Wheel"));                                  } break;
        case InputEventKind_MouseMove:          { result = push_string_copy(arena, string_u8_litexpr("Mouse Move"));                                   } break;
        case InputEventKind_Core:               { result = push_u8_stringf (arena, "Core %s", ArraySafe(core_code_name, trigger->sub_code));           } break;
        
        default: { result = push_string_copy(arena, string_u8_litexpr("ERROR unexpected trigger kind")); } break;
    }
    
    return result;
}


//
// HUD generation
//

function void
mod_push_fancy_command_trigger_line(Application_Links *app, Mod_Bindings_Hud_Block *block, Mod_Fancy_Command_Trigger_Line *line){
    sll_queue_push(block->first, block->last, line);
    line->width = get_fancy_line_width(app, block->face, &line->line);
    if(line->line.first == line->line.last) {
        block->title_count += 1;
    } else {
        line->trigger_width = line->width - get_fancy_string_width(app, block->face, line->line.last);
        
        block->trigger_width = Max(block->trigger_width, line->trigger_width);
        block->trigger_count += 1;
    }
    block->width = Max(block->width, line->width);
}

function void
mod_generate_fancy_map_binding_block(Application_Links *app, Arena *arena,
                                     Mod_Bindings_Hud_Block *block,
                                     Table_Data_u64 *trigger_table, Command_Map *map) {
    for(Command_Modified_Binding *binding_node = map->binding_first;
        binding_node != 0;
        binding_node = binding_node->next) {
        Command_Trigger_List triggers = map_get_triggers_non_recursive(map, binding_node->binding);
        Command_Metadata *meta = get_command_metadata(binding_node->binding.custom);
        if(!meta || (meta->proc == project_fkey_command)) continue;
        
        Mod_Fancy_Command_Trigger_Line *trigger_line = 0;
        for (Command_Trigger *trigger_node = triggers.first;
             trigger_node != 0;
             trigger_node = trigger_node->next){
            
            if((trigger_node->kind == InputEventKind_None) ||
               (trigger_node->kind == InputEventKind_Core)) continue;
            
            // NOTE(NJ): We want to free strings that we aren't going to use,
            // so we sneakily begin this temporary memory here, but end it
            // only when we found this trigger is already in the table.
            Temp_Memory trigger_temp = begin_temp(arena);
            
            String_Const_u8 mods_string    = mod_command_trigger_stringize_mods(arena, trigger_node);
            String_Const_u8 trigger_string = mod_command_trigger_stringize     (arena, trigger_node);
            
            Temp_Memory table_key_temp = begin_temp(arena);
            String_u8 key = push_string_u8(arena, mods_string.size + trigger_string.size);
            string_append(&key, mods_string);
            string_append(&key, trigger_string);
            
            b32 new_trigger = table_insert(trigger_table, SCu8(key), 1);
            end_temp(table_key_temp);
            
            if(new_trigger) {
                if(trigger_line == 0) {
                    trigger_line = push_array_zero(arena, Mod_Fancy_Command_Trigger_Line, 1);
                }  else {
                    push_fancy_string(arena, &trigger_line->line, block->colors.separator, string_u8_litexpr(" / "));
                }
                
                push_fancy_string(arena, &trigger_line->line, block->colors.mods, mods_string);
                push_fancy_string(arena, &trigger_line->line, block->colors.keys, trigger_string);
            } else {
                end_temp(trigger_temp);
            }
        }
        if(trigger_line == 0) continue;
        
        trigger_line->meta = meta;
        push_fancy_string(arena, &trigger_line->line, fcolor_zero(), string_u8_litexpr("  "));
        
        push_fancy_string(arena, &trigger_line->line, block->colors.base, SCu8(meta->name, meta->name_len));
        mod_push_fancy_command_trigger_line(app, block, trigger_line);
    }
}

function void
mod_fill_fancy_command_trigger_block(Application_Links *app, Mod_Bindings_Hud_State *state, View_ID view) {
    Mod_Bindings_Hud_Block *block = &state->block;
    mod_fill_binding_hud_colors(&state->block.colors);
    
    View_Context ctx = view_current_context(app, view);
    Mapping *mapping = ctx.mapping;
    // global_active_mapid is the actual map id my layer uses, replace with whatever
    // your layer uses (I think it's usually just ctx.map_id, although I didn't check
    // with the vanilla 4coder version) :BindingHudModalLayerSpecific:
    Command_Map *map = mapping_get_map(mapping, state->mapid);
    Arena *arena = &state->arena;
    
    // Write the name of the active map
    {
        Mod_Fancy_Command_Trigger_Line *line = push_array_zero(arena, Mod_Fancy_Command_Trigger_Line, 1);
        String_Const_u8 map_name = vars_read_string(arena, map->id);
        push_fancy_string(arena, &line->line, block->colors.title1, map_name);
        mod_push_fancy_command_trigger_line(app, block, line);
    }
    
    if(map->text_input_command.custom) {
        Command_Metadata *meta = get_command_metadata(map->text_input_command.custom);
        if(meta) {
            Mod_Fancy_Command_Trigger_Line *line = push_array_zero(arena, Mod_Fancy_Command_Trigger_Line, 1);
            push_fancy_string(arena, &line->line, block->colors.keys, string_u8_litexpr("Text Input  "));
            push_fancy_string(arena, &line->line, block->colors.base, SCu8(meta->name, meta->name_len));
            line->meta = meta;
            mod_push_fancy_command_trigger_line(app, block, line);
        }
    }
    
    mod_generate_fancy_map_binding_block(app, arena, block, &state->trigger_table, map);
    
    for(Command_Map *parent = mapping_get_map(mapping, map->parent);
        parent; parent = mapping_get_map(mapping, parent->parent)) {
        Mod_Fancy_Command_Trigger_Line *line = push_array_zero(arena, Mod_Fancy_Command_Trigger_Line, 1);
        String_Const_u8 mode_name = vars_read_string(arena, parent->id);
        push_fancy_stringf(arena, &line->line, block->colors.title2, "Inherited from %.*s:", string_expand(mode_name));
        mod_push_fancy_command_trigger_line(app, block, line);
        
        mod_generate_fancy_map_binding_block(app, arena, block, &state->trigger_table, parent);
    }
    
    // Calculate the block width including the padding for the trigger column
    for(Mod_Fancy_Command_Trigger_Line *line = block->first;
        line != 0;
        line = line->next) {
        if(line->meta) {
            f32 width = line->width + (block->trigger_width - line->trigger_width);
            block->width = Max(block->width, width);
        }
    }
}


//
// HUD drawing
//

function void
mod_draw_bindings_hud(Application_Links *app, View_ID view_id, Buffer_ID buffer, Face_ID face_id) {
    ProfileScope(app, "draw command bindings hud");
    Scratch_Block scratch(app);
    
    Mod_Bindings_Hud_State *state = &global_bindings_hud_state;
    
    if(state->arena.base_allocator == 0) {
        state->arena = make_arena_system();
    }
    
    Face_Metrics metrics = get_face_metrics(app, face_id);
    
    // If we changed a keyboard mapping from the last time we were here, 
    // we want to generate the text for the hud again.
    // You can replace this with whatever your layer uses to get the active mapping id.
    // :BindingHudModalLayerSpecific: 
    if(state->mapid != global_active_mapid) {
        state->mapid = global_active_mapid;
        linalloc_clear(&state->arena);
        
        // The trigger table is used for showing only the actual keys that are available from
        // the active map, so if a binding of a parent map is overwritten it will not be shown
        // in the HUD.
        const u64 probably_large_enough = 256;
        state->trigger_table = make_table_Data_u64(state->arena.base_allocator, probably_large_enough);
        state->block = {};
        state->block.face = face_id;
        mod_fill_fancy_command_trigger_block(app, state, view_id);
    }
    
    mod_fill_binding_hud_colors(&state->block.colors);
    
    Mod_Bindings_Hud_Block *block = &state->block;
    
    // Keep the rendering correct with variable font size {
    f32 face_size_ratio = get_fancy_line_width(app, face_id, &block->first->line)/block->first->width;
    f32 line_advance    = metrics.line_height;
    f32 block_height    = (block->title_count*line_advance*1.6f) + (block->trigger_count*line_advance);
    f32 block_width     = block->width*face_size_ratio;
    f32 trigger_width   = block->trigger_width*face_size_ratio;
    // }
    
    f32 margin = metrics.line_height;
    Rect_f32 view_rect = view_get_screen_rect(app, view_id);
    Vec2_f32 mid = V2f32(view_rect.x1 - view_rect.x0, view_rect.y1 - view_rect.y0)/2.f;
    Rect_f32 region;
    region.x0 = view_rect.x0 + mid.x - (block_width  + margin)/2.f;
    region.x1 = view_rect.x0 + mid.x + (block_width  + margin)/2.f;
    region.y0 = view_rect.y0 + mid.y - (block_height + margin)/2.f;
    region.y1 = view_rect.y0 + mid.y + (block_height + margin + line_advance/2.f)/2.f;
    
    if(rect_height(region) > rect_height(view_rect)) {
        f32 max_scroll = (rect_height(region) - rect_height(view_rect)) + margin/2.f;
        state->hud_base_y = clamp(0.f, state->hud_base_y, max_scroll);
        region.y0 = view_rect.y0 - state->hud_base_y + margin/2.f;
        region.y1 = region.y0 + block_height + (line_advance/2.f + margin)/2.f;
    }
    
    Rect_f32 clip = draw_set_clip(app, view_rect);
    draw_rectangle_outline(app, region, 4.f, 6.f, fcolor_resolve(fcolor_id(defcolor_margin)));
    draw_rectangle(app, region, 4.f, fcolor_resolve(block->colors.back));
    
    Vec2_f32 p0 = region.p0 + V2f32(margin, margin)/2.f;
    Mouse_State mouse = get_mouse_state(app);
    Vec2_f32 mouse_p = V2f32(mouse.p);
    
    String_Const_u8 description = {};
    
    for(Mod_Fancy_Command_Trigger_Line *line = block->first;
        line != 0;
        line = line->next) {
        Vec2_f32 p = p0;
        f32 line_width = line->width*face_size_ratio;
        f32 line_trigger_width = line->trigger_width*face_size_ratio;
        
        if(line->meta == 0) {
            // A title line
            p.x += (block_width - line_width)/2.f;
            p.y += line_advance*.35f;
            
            Rect_f32 underline;
            underline.y0 = p.y + line_advance*.95f;
            underline.y1 = underline.y0 + 1.f;
            underline.x0 = p0.x + margin/2.f;
            underline.x1 = p0.x + block_width - margin/2.f;
            FColor underline_color = fcolor_change_alpha(line->line.first->fore, .75f);
            draw_rectangle(app, underline, 3.f, fcolor_resolve(underline_color));
            
            p0.y += line_advance*.6f;
        } else {
            // A trigger line
            p.x += (trigger_width - line_trigger_width);
            Rect_f32 hot_region;
            hot_region.x0 = region.x0 + trigger_width - 3.f*metrics.space_advance/2.f;
            hot_region.x1 = p.x + line_width + margin/2.f;
            hot_region.y0 = p0.y;
            hot_region.y1 = p0.y + line_advance;
            
            if(rect_contains_point(hot_region, mouse_p)) {
                // If the mouse is in the hot region, highlight the line and setup
                // its description to be drawn
                description = SCu8(line->meta->description, line->meta->description_len);
                
                state->hot_command_meta = line->meta;
                line->line.last->fore = block->colors.title1;
                
                Rect_f32 hot_line_dot = {};
                f32 dot_dim = 2.f*metrics.space_advance/3.f;
                hot_line_dot.x0 = p0.x + trigger_width - metrics.space_advance - dot_dim/2.f;
                hot_line_dot.x1 = hot_line_dot.x0 + dot_dim;
                hot_line_dot.y0 = p0.y + metrics.line_height/2.f - dot_dim/2.f;
                hot_line_dot.y1 = hot_line_dot.y0 + dot_dim;
                
                draw_rectangle(app, hot_line_dot, 5.f, fcolor_resolve(line->line.last->fore));
            } else {
                line->line.last->fore = block->colors.base;
            }
        }
        
        draw_fancy_line(app, face_id, fcolor_zero(), &line->line, p);
        p0.y += line_advance;
    }
    
    if(description.str && description.size > 0) {
        List_String_Const_u8 wrapped_desc = {};
        f32 desc_width = rect_width(view_rect) - 2.f*margin;
        Vec2_f32 desc_dims = mod_wrap_text_to_width(app, scratch, &wrapped_desc,  description,
                                                    block->face, desc_width, line_advance);
        
        Rect_f32 desc_rect;
        desc_rect.y0 = view_rect.y1 - desc_dims.y - 3.f*margin/2.f;
        desc_rect.y1 = view_rect.y1 - margin/2.f;
        desc_rect.x0 = view_rect.x0 + margin/2.f;
        desc_rect.x1 = view_rect.x0 + desc_dims.x + metrics.normal_advance + margin;
        draw_rectangle_outline(app, desc_rect, 4.f, 6.f, fcolor_resolve(block->colors.margin));
        
        draw_rectangle(app, desc_rect, 4.f, fcolor_resolve(block->colors.description_back));
        
        Vec2_f32 p = desc_rect.p0 + V2f32(margin, margin)/2.f;
        for(auto *wrapped_line = wrapped_desc.first;
            wrapped_line != 0;
            wrapped_line = wrapped_line->next) {
            Fancy_String fancy_wrapped_line = {};
            fancy_wrapped_line.value = wrapped_line->string;
            fancy_wrapped_line.fore = block->colors.base;
            draw_fancy_string(app, face_id, block->colors.base, &fancy_wrapped_line, p);
            p.y += line_advance;
        }
    } else {
        state->hot_command_meta = 0;
    }
    
    draw_set_clip(app, clip);
}

#endif // _MODAL_BINDINGS_HUD_CPP