#if !defined(_MODAL_BINDINGS_HUD_H)
#define _MODAL_BINDINGS_HUD_H

struct Mod_Fancy_Command_Trigger_Line {
    Fancy_Line line;
    Mod_Fancy_Command_Trigger_Line *next;
    Command_Metadata *meta;
    f32 width, trigger_width;
};

struct Mod_Bindings_Hud_Colors {
    FColor back;
    FColor base;
    FColor keys;
    FColor mods;
    FColor cursor;
    FColor margin;
    FColor title1;
    FColor title2;
    FColor separator;
    FColor description_back;
};

struct Mod_Bindings_Hud_Block {
    Face_ID face;
    f32 width, trigger_width;
    
    Mod_Fancy_Command_Trigger_Line *first;
    Mod_Fancy_Command_Trigger_Line *last;
    i32 trigger_count;
    i32 title_count;
    
    Mod_Bindings_Hud_Colors colors;
};

struct Mod_Bindings_Hud_State {
    Arena arena;
    Table_Data_u64 trigger_table; // For not printing multiple bindings of the same keys
    Command_Map_ID mapid;
    
    Mod_Bindings_Hud_Block block;
    
    b32 show_hud;
    f32 hud_base_y;
    Command_Metadata *hot_command_meta;
};

global Mod_Bindings_Hud_State global_bindings_hud_state = {};

#endif // _MODAL_BINDINGS_HUD_H