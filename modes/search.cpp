#if !defined(_MOD_SEARCH_CPP)
#define _MOD_SEARCH_CPP

CUSTOM_COMMAND_SIG(modal_set_mode_search)
CUSTOM_DOC("Activates 'search' mode.") {
    Search_State *search = &global_search_state;
    search->query.view = get_active_view(app, Access_ReadVisible);
    search->query.buffer = view_get_buffer(app, search->query.view, Access_ReadVisible);
    
    if(!view_exists(app, search->query.view) || !buffer_exists(app, search->query.buffer)) {
        modal_revert_mode(app);
        return;
    }
    
    search->match = Ii64(view_get_cursor_pos(app, search->query.view));
    search->direction = Scan_Forward;
    search->last_cursor = buffer_compute_cursor(app, search->query.buffer, seek_pos(search->match.min));
    search->do_highlights = true;
    
    Query_State *query = &search->query;
    query_start(app, query, mode_search);
    
    query->prompt[0] = string_u8_litinit("Search: ");
    
    query->update_hook = search_update_query;
    query->apply_hook  = search_apply;
    query->cancel_hook = search_cancel;
}

CUSTOM_COMMAND_SIG(modal_set_mode_search_on_token)
CUSTOM_DOC("Activates 'mode' chord on the token under the active cursor.") {
    modal_set_mode_search(app);
    
    Search_State *search = &global_search_state;
    Range_i64 match = mod_get_token_range_under_cursor(app, Scan_Forward);
    search->match = range_clamp_size(match, MOD_QUERY_CAP);
    buffer_read_range(app, search->query.buffer, search->match, search->query.string[0].str);
    search->query.string[0].size = range_size(search->match);
    
    query_apply(app);
}

function void modal_bind_mode_search(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(mode_search);
    ParentMap(mode_query);
    
    Bind(search_find_next, KeyCode_PageDown);
    Bind(search_find_next, KeyCode_J, KeyCode_Control);
    Bind(search_find_next, KeyCode_N, KeyCode_Control);
    
    Bind(search_find_prev, KeyCode_PageUp);
    Bind(search_find_prev, KeyCode_K, KeyCode_Control);
    Bind(search_find_prev, KeyCode_N, KeyCode_Control, KeyCode_Shift);
    
    Bind(search_word_complete, KeyCode_Tab);
    
    Bind(search_list_all_substring_locations, KeyCode_T, KeyCode_Control);
}

CUSTOM_COMMAND_SIG(modal_set_theme_mode_search)
CUSTOM_DOC("Sets the theme for 'search' mode.") {
    MODAL_THEME_SET_COLOR(back,          0, 0xff051510);
    MODAL_THEME_SET_COLOR(margin,        0, 0xff034449);
    MODAL_THEME_SET_COLOR(margin_hover,  0, 0xff021f4f);
    MODAL_THEME_SET_COLOR(margin_active, 0, 0xff055459);
    MODAL_THEME_SET_COLOR(cursor, 0, 0xff207469);
    MODAL_THEME_SET_COLOR(mark,   0, 0xff0d6695);
    
    MODAL_THEME_COPY_COLOR(bar, 0, margin, 0);
    
    MODAL_THEME_SET_ALPHA(bar,       0, .5f);
    MODAL_THEME_SET_ALPHA(highlight, 0, 1.f);
}

function void search_update(Application_Links *app, b32 query_changed, String_Match_Flag match_flags) {
    Search_State *search = &global_search_state;
    Query_State  *query  = &search->query;
    
    if(!buffer_exists(app, query->buffer)){
        modal_revert_mode(app);
        return;
    }
    
    i64 buffer_size = buffer_get_size(app, query->buffer);
    i64 offset = 0;
    if(query_changed) offset = 1;
    
    i64 new_pos = search->match.min;
    Buffer_Seek_String_Flags flag = 0;
    if(search->direction == Scan_Backward) flag |= BufferSeekString_Backward;
    if(!(match_flags & StringMatch_CaseSensitive)) flag |= BufferSeekString_CaseInsensitive;
    String_u8 needle = query->string[query->stage];
    seek_string(app, query->buffer, search->match.min - offset, 0, 0, SCu8(needle), &new_pos, flag);
    
    if(0 <= new_pos && new_pos < buffer_size) {
        search->match = Ii64_size(new_pos, needle.size);
        view_set_cursor(app, query->view, seek_pos(search->match.max));
    }
}

function void search_update(Application_Links *app, b32 query_changed) {
    Search_State *search = &global_search_state;
    search_update(app, query_changed, search->query.match_flags);
}

CUSTOM_COMMAND_SIG(search_update_query)
CUSTOM_DOC("Updates the cursor position when the search query changes.") {
    search_update(app, true);
}

CUSTOM_COMMAND_SIG(search_step_query) {
    search_update(app, false);
}

CUSTOM_COMMAND_SIG(search_find_next)
CUSTOM_DOC("Jumps to the next search result."){
    Search_State *search = &global_search_state;
    search->direction = Scan_Forward;
    search_step_query(app);
}

CUSTOM_COMMAND_SIG(search_find_prev)
CUSTOM_DOC("Jumps to the previous search result."){
    Search_State *search = &global_search_state;
    search->direction = Scan_Backward;
    search_step_query(app);
}

CUSTOM_COMMAND_SIG(search_find_or_jump_next)
CUSTOM_DOC("Jumps to the next location in the jump buffer, or to the next search result."){
    Heap *heap = &global_heap;
    
    Locked_Jump_State jump_state = get_locked_jump_state(app, heap);
    i32 jump_count = 0;
    if(jump_state.view != 0) {
        i64 cursor_position = view_get_cursor_pos(app, jump_state.view);
        Buffer_Cursor cursor = view_compute_cursor(app, jump_state.view, seek_pos(cursor_position));
        i64 line = get_line_from_list(app, jump_state.list, jump_state.list_index);
        if(jump_state.list) {
            jump_count = jump_state.list->jump_count;
        }
        if (line <= cursor.line){
            jump_state.list_index += 1;
        }
        goto_next_filtered_jump(app, jump_state.list, jump_state.view, jump_state.list_index, 1, true, true);
    }
    if(jump_count == 0) {
        Search_State *search = &global_search_state;
        
        search->query.view = get_active_view(app, Access_ReadVisible);
        search->query.buffer = view_get_buffer(app, search->query.view, Access_ReadVisible);
        i64 pos = view_get_cursor_pos(app, search->query.view);
        search->match = Ii64(pos);
        search_find_next(app);
    }
}

CUSTOM_COMMAND_SIG(search_find_or_jump_prev)
CUSTOM_DOC("Jumps to the previous location in the jump buffer, or to the previous search result."){
    Heap *heap = &global_heap;
    
    Locked_Jump_State jump_state = get_locked_jump_state(app, heap);
    i32 jump_count = 0;
    if(jump_state.view != 0) {
        if (jump_state.list_index > 0){
            --jump_state.list_index;
        }
        if(jump_state.list) {
            jump_count = jump_state.list->jump_count;
        }
        goto_next_filtered_jump(app, jump_state.list, jump_state.view, jump_state.list_index, -1, true, true);
    }
    if(jump_count == 0) {
        Search_State *search = &global_search_state;
        
        search->query.view = get_active_view(app, Access_ReadVisible);
        search->query.buffer = view_get_buffer(app, search->query.view, Access_ReadVisible);
        i64 pos = view_get_cursor_pos(app, search->query.view);
        search->match = Ii64(pos);
        search_find_prev(app);
        if(search->match.max == pos) search_find_prev(app);
    }
}

CUSTOM_COMMAND_SIG(search_word_complete)
CUSTOM_DOC("Completes the word in the search query based on the open buffers."){
    Search_State *search = &global_search_state;
    Query_State  *query  = &search->query;
    b32 first_completion = (query->completion_pos == -1);
    if(range_size(search->match) == 0) return;
    
    if(first_completion) {
        query->completion_pos = search->match.max;
    }
    
    String_Const_u8 completion = buffer_get_word_completion(app, query->buffer, first_completion, search->match, query->completion_pos);
    if(completion.size > 0) {
        block_copy(query->string[query->stage].str, completion.str, completion.size);
        query->string[query->stage].size = completion.size;
        query->selection.min = (i64)query->string[query->stage].size;
        query->selection.max = (i64)query->string[query->stage].size;
        search_update(app, true, query->match_flags | StringMatch_CaseSensitive);
    } else {
        view_set_cursor(app, search->query.view, seek_pos(search->last_cursor.pos));
    }
}

CUSTOM_COMMAND_SIG(search_cancel)
CUSTOM_DOC("Cancels the search mode and places the cursor in its initial position."){
    Search_State *search = &global_search_state;
    search->do_highlights = false;
    view_set_cursor(app, search->query.view, seek_pos(search->last_cursor.pos));
}

CUSTOM_COMMAND_SIG(search_apply)
CUSTOM_DOC("Applys the search query and saved the last searched string."){
    unlock_jump_buffer();
}

CUSTOM_COMMAND_SIG(search_list_all_substring_locations)
CUSTOM_DOC("Like list_all_substring_locations but from the search query (can be case sensitive, depending on the search search)."){
    b32 in_query = modal_active_mode_inherits_from(app, mode_query);
    
    if(!in_query) {
        start_search_like_query(app, string_u8_litexpr("List Locations For: "),
                                search_list_all_substring_locations);
    } else {
        Query_State *query = active_query_state;
        List_All_Locations_Flag flag = ListAllLocationsFlag_MatchSubstring;
        if(query->match_flags & StringMatch_CaseSensitive) {
            flag |= ListAllLocationsFlag_CaseSensitive;
        }
        
        list_all_locations__generic(app, SCu8(query->string[query->stage]), flag);
    }
}

CUSTOM_COMMAND_SIG(mod_query_replace)
CUSTOM_DOC("See query_replace.") {
    b32 in_query = modal_active_mode_inherits_from(app, mode_query);
    
    if(!in_query) {
        start_search_like_query(app, string_u8_litexpr("Replace: "), mod_query_replace);
    } else {
        Query_State *query = active_query_state;
        if(query->stage == 0) {
            query_next_stage(app);
            query->prompt[query->stage] = string_u8_litexpr("With: ");
        } else {
            i64 pos = view_get_cursor_pos(app, query->view);
            
            Query_Bar_Group group(app);
            Query_Bar bar = {};
            bar.prompt = string_u8_litexpr("Replace? (y)es, (n)ext, (esc)\n");
            start_query_bar(app, &bar, 0);
            
            query_replace_base(app, query->view, query->buffer,
                               pos, SCu8(query->string[0]), SCu8(query->string[1]));
        }
    }
}

CUSTOM_COMMAND_SIG(mod_query_replace_identifier)
CUSTOM_DOC("See mod_query_replace_identifier.") {
    Query_State *query = &global_search_state.query;
    query_start(app, query, mode_query, true);
    
    modal_set_theme(app, mode_search);
    global_search_state.do_highlights = true;
    
    query->apply_hook  = mod_query_replace;
    query->update_hook = 0;
    query->cancel_hook = 0;
    mod_query_replace(app);
}

CUSTOM_COMMAND_SIG(mod_replace_in_range)
CUSTOM_DOC("See replace_in_range.") {
    b32 in_query = modal_active_mode_inherits_from(app, mode_query);
    
    if(!in_query) {
        start_search_like_query(app, string_u8_litexpr("Replace: "), mod_replace_in_range);
    } else {
        Query_State *query = active_query_state;
        if(query->stage == 0) {
            query_next_stage(app);
            query->prompt[query->stage] = string_u8_litexpr("With: ");
        } else {
            Range_i64 range = get_view_range(app, query->view);
            replace_in_range(app, query->buffer, range, SCu8(query->string[0]), SCu8(query->string[1]));
        }
    }
}

CUSTOM_COMMAND_SIG(search_disable_highlights)
CUSTOM_DOC("Disables the search highlights.") {
    global_search_state.do_highlights = false;
}

#endif // _MOD_SEARCH_CPP