#if !defined(_MOD_SNIPPET_CPP)
#define _MOD_SNIPPET_CPP

// 
// HACK(FS): NOTE(FS): for multiline command documentation, we need to fix a bug in 
//   4coder_metadata_generator.cpp, where it prints the line width including escaped
//   characters, i.e. thinks that "\n" is actually 2 characters long.
//   As of 4coder beta 4.1.8, this behaviour can be fix by replacing 
//   4coder_metadata_generator.cpp lines 913-924 with the following snippet (hack):
//
/*
     auto str = entry->docstring.doc;
     u64 docstring_size = str.size;
     for(u64 k = 0; k < str.size; ++k) {
         if(str.str[k] == '\\') {
             docstring_size -= 1;
             if((k + 1 < str.size) && str.str[k + 1] == '\\') ++k;
         }
     }
     
     fprintf(cmd_out,
             "{ PROC_LINKS(%.*s, 0), %s, \"%.*s\", %d, "
             "\"%.*s\", %d, \"%s\", %d, %" FMTi64 " },\n",
             string_expand(entry->name),
             is_ui,
             string_expand(entry->name),
             (i32)entry->name.size,
             string_expand(entry->docstring.doc),
     ---->   (i32)docstring_size,
             printable.str,
             (i32)source_name.size,
             entry->line_number);
*/

CUSTOM_COMMAND_SIG(modal_set_chord_snippet)
CUSTOM_DOC("Activates 'snippet' chord.") {
    modal_set_active_mapid(app, chord_snippet);
    mod_show_bindings_hud(app);
}

function void modal_bind_chord_snippet(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(chord_snippet);
    ParentMap(mapid_vital);
    
    Bind(snippet_chord_write_if,     KeyCode_I);
    Bind(snippet_chord_write_elseif, KeyCode_I, KeyCode_Shift);
    Bind(snippet_chord_write_elseif, KeyCode_E, KeyCode_Shift);
    Bind(snippet_chord_write_else,   KeyCode_E);
    
    Bind(snippet_chord_write_fori, KeyCode_F);
    Bind(snippet_chord_write_for,  KeyCode_F, KeyCode_Shift);
    
    Bind(snippet_chord_write_while,   KeyCode_W);
    Bind(snippet_chord_write_dowhile, KeyCode_D);
    
    Bind(snippet_chord_write_assert, KeyCode_A);
    
    Bind(snippet_chord_write_todo, KeyCode_T);
    Bind(snippet_chord_write_note, KeyCode_N);
    
    Bind(snippet_chord_write_include_gaurd, KeyCode_3, KeyCode_Shift);
    
    Bind(modal_revert_mode, KeyCode_Escape);
}

CUSTOM_COMMAND_SIG(modal_set_theme_chord_snippet)
CUSTOM_DOC("Sets the theme for 'snippet' chord.") {
    modal_set_theme_chord_insert(app);
}

#define SNIPPET_COMMAND(name, snippet, cpos, mpos) \
global Snippet global_snippet_ ## name = {#name, snippet, cpos, mpos}; \
CUSTOM_COMMAND_SIG(snippet_chord_ ## name) \
CUSTOM_DOC(snippet){ \
View_ID view = get_this_ctx_view(app, Access_ReadWrite); \
if(view == 0) return; \
Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWriteVisible); \
\
History_Group history_group = history_group_begin(app, buffer); \
seek_end_of_textual_line(app); \
i64 pos = view_get_cursor_pos(app, view); \
write_snippet(app, view, buffer, pos, &global_snippet_ ## name); \
history_group_end(history_group); \
\
modal_set_mode_insert(app); \
}

SNIPPET_COMMAND(write_if,      "if() {\n\n}\n",                         3,  3);
SNIPPET_COMMAND(write_elseif,  " else if() {\n\n}",                      9,  9);
SNIPPET_COMMAND(write_else,    " else {\n\n}",                           8,  8);
SNIPPET_COMMAND(write_for,     "for() {\n\n}\n",                        4,  4);
SNIPPET_COMMAND(write_fori,    "for(int i = 0; i < ; i += 1){\n\n}\n", 19, 19);
SNIPPET_COMMAND(write_while,   "while() {\n\n}\n",                      6,  6);
SNIPPET_COMMAND(write_dowhile, "do {\n\n} while();\n",                  5,  5);
SNIPPET_COMMAND(write_assert,  "assert();\n",                           7,  7);

#undef SNIPPET_COMMAND

CUSTOM_COMMAND_SIG(snippet_chord_write_todo)
CUSTOM_DOC(" // TODO: "){
    View_ID view = get_active_view(app, Access_ReadVisible);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWriteVisible);
    
    History_Group history_group = history_group_begin(app, buffer);
    seek_end_of_textual_line(app);
    write_text(app, string_u8_litexpr(" "));
    write_todo(app);
    history_group_end(history_group);
    
    modal_set_mode_insert(app);
}

CUSTOM_COMMAND_SIG(snippet_chord_write_note)
CUSTOM_DOC(" // NOTE:"){
    View_ID view = get_active_view(app, Access_ReadVisible);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWriteVisible);
    
    History_Group history_group = history_group_begin(app, buffer);
    seek_end_of_textual_line(app);
    write_text(app, string_u8_litexpr(" "));
    write_note(app);
    history_group_end(history_group);
    
    modal_set_mode_insert(app);
}

CUSTOM_COMMAND_SIG(snippet_chord_write_include_gaurd)
CUSTOM_DOC("#if !defined(_FINE_NAME)\n#define _FINE_NAME\n\n\n#endif // _FINE_NAME"){
    mod_include_gaurd(app);
    modal_revert_mode(app);
}

#endif // _MOD_SNIPPET_CPP