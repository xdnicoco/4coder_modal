#if !defined(_MOD_INSERT_CPP)
#define _MOD_INSERT_CPP

//
// Mode Insert
//
CUSTOM_COMMAND_SIG(modal_set_mode_insert)
CUSTOM_DOC("Activates 'insert' mode.") {
    if(mod_active_buffer_has_write_access(app))
        modal_set_active_mapid(app, mode_insert);
}

function void modal_bind_mode_insert(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(mode_insert);
    ParentMap(mapid_common);
    Bind(modal_set_mode_normal,   KeyCode_Escape);
    Bind(modal_set_chord_snippet, KeyCode_3, KeyCode_Control);
    BindTextInput(write_text_input);
}

CUSTOM_COMMAND_SIG(modal_set_theme_mode_insert)
CUSTOM_DOC("Sets the theme for 'insert' mode.") {
    MODAL_THEME_SET_COLOR(back,          0, 0xff100f05);
    MODAL_THEME_SET_COLOR(margin,        0, 0xff711400);
    MODAL_THEME_SET_COLOR(margin_hover,  0, 0xff752410);
    MODAL_THEME_SET_COLOR(margin_active, 0, 0xff793420);
    MODAL_THEME_COPY_COLOR(bar, 0, margin, 0);
    
    MODAL_THEME_SET_COLOR(cursor, 0, 0xff793420);
    MODAL_THEME_SET_COLOR(mark,   0, 0xff8d360d);
    MODAL_THEME_SET_COLOR(pop1,   0, 0xff55cccc);
    MODAL_THEME_SET_COLOR(pop2,   0, 0xff5591cc);
}

CUSTOM_COMMAND_SIG(modal_newline_before_then_insert)
CUSTOM_DOC("Inserts a newline before the line under the cursor, then activates insert mode."){
    seek_beginning_of_line(app);
    write_text(app, string_u8_litexpr("\n"));
    move_left(app);
    modal_set_mode_insert(app);
}

CUSTOM_COMMAND_SIG(modal_newline_after_then_insert)
CUSTOM_DOC("Inserts a newline after the line under the cursor, then activates insert mode."){
    seek_end_of_line(app);
    write_text(app, string_u8_litexpr("\n"));
    modal_set_mode_insert(app);
}

CUSTOM_COMMAND_SIG(modal_seek_eol_then_insert)
CUSTOM_DOC("Moves to the end of the current line, then activates insert mode."){
    seek_end_of_textual_line(app);
    modal_set_mode_insert(app);
}

//
// Chord Insert
//
CUSTOM_COMMAND_SIG(modal_set_chord_insert)
CUSTOM_DOC("Activates 'insert' mode.") {
    if(mod_active_buffer_has_write_access(app))
        modal_set_active_mapid(app, chord_insert);
}

function void modal_bind_chord_insert(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(chord_insert);
    ParentMap(mapid_vital);
    Bind(modal_set_mode_normal, KeyCode_Escape);
    BindTextInput(insert_chord_handle_input);
}

CUSTOM_COMMAND_SIG(modal_set_theme_chord_insert)
CUSTOM_DOC("Sets the theme for 'insert' chord.") {
    MODAL_THEME_SET_COLOR(margin,        0, 0xff032459);
    MODAL_THEME_SET_COLOR(margin_hover,  0, 0xff021f4f);
    MODAL_THEME_SET_COLOR(margin_active, 0, 0xffb74430);
    MODAL_THEME_COPY_COLOR(bar, 0, margin, 0);
    
    MODAL_THEME_SET_COLOR(cursor, 0, 0xffe95420);
    MODAL_THEME_SET_COLOR(mark,   0, 0xff8d360d);
}

CUSTOM_COMMAND_SIG(insert_chord_handle_input)
CUSTOM_DOC("Inserts whatever character was used to trigger this command."){
    write_text_input(app);
    modal_revert_mode(app);
}

#endif // _MOD_INSERT_CPP