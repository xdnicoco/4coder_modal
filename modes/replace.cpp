#if !defined(_MOD_REPLACE_CPP)
#define _MOD_REPLACE_CPP

//
// Mode Replace
//
CUSTOM_COMMAND_SIG(modal_set_mode_replace)
CUSTOM_DOC("Activates 'replace' mode.") {
    if(mod_active_buffer_has_write_access(app))
        modal_set_active_mapid(app, mode_replace);
}

function void modal_bind_mode_replace(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(mode_replace);
    ParentMap(mapid_common);
    
    Bind(modal_revert_mode, KeyCode_Insert);
    Bind(undo, KeyCode_Backspace);
    
    Bind(replace_paste,             KeyCode_V, KeyCode_Control);
    Bind(replace_paste_next,        KeyCode_V, KeyCode_Control, KeyCode_Shift);
    //Bind(replace_rectangular_paste, KeyCode_V, KeyCode_Control, KeyCode_Alt);
    
    Bind(modal_set_mode_normal, KeyCode_Escape);
    BindTextInput(replace_handle_input);
}

CUSTOM_COMMAND_SIG(modal_set_theme_mode_replace)
CUSTOM_DOC("Sets the theme for 'replace' mode.") {
    MODAL_THEME_SET_COLOR(back,          0, 0xff0f0515);
    MODAL_THEME_SET_COLOR(margin,        0, 0xff311b92);
    MODAL_THEME_SET_COLOR(margin_hover,  0, 0xff332ba2);
    MODAL_THEME_SET_COLOR(margin_active, 0, 0xff353bb2);
    MODAL_THEME_COPY_COLOR(bar, 0, margin, 0);
    
    MODAL_THEME_SET_COLOR(cursor, 0, 0xffe91e63);
    MODAL_THEME_SET_COLOR(mark,   0, 0xffc30c4a);
    MODAL_THEME_SET_COLOR(pop1,   0, 0xff27b4d4);
    MODAL_THEME_SET_COLOR(pop2,   0, 0xffd49e27);
}

CUSTOM_COMMAND_SIG(replace_write_char)
CUSTOM_DOC("Replaces the character under the curser with the current input."){
    User_Input in = get_current_input(app);
    String_Const_u8 insert = to_writable(&in);
    if(!insert.str || insert.size == 0) return;
    
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    i64 pos = view_get_cursor_pos(app, view);
    i64 line_end = get_line_end_pos(app, buffer, get_line_number_from_pos(app, buffer, pos));
    
    i64 len = 1;
    if(pos == line_end) len = 0;
    
    buffer_replace_range(app, buffer, {pos, pos + len}, insert);
}

CUSTOM_COMMAND_SIG(replace_paste)
CUSTOM_DOC("Paste and replaces the input if possible."){
    clipboard_update_history_from_system(app, 0);
    i32 count = clipboard_count(0);
    if (count > 0){
        View_ID view = get_active_view(app, Access_ReadWriteVisible);
        if_view_has_highlighted_range_delete_range(app, view);
        
        Managed_Scope scope = view_get_managed_scope(app, view);
        Rewrite_Type *next_rewrite = scope_attachment(app, scope, view_next_rewrite_loc, Rewrite_Type);
        if (next_rewrite != 0){
            *next_rewrite = Rewrite_Paste;
            i32 *paste_index = scope_attachment(app, scope, view_paste_index_loc, i32);
            *paste_index = 0;
            
            Scratch_Block scratch(app);
            
            String_Const_u8 string = push_clipboard_index(scratch, 0, *paste_index);
            if (string.size > 0){
                Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWriteVisible);
                
                i64 pos = view_get_cursor_pos(app, view);
                buffer_replace_range(app, buffer, Ii64(pos, pos + string.size), string);
                view_set_mark(app, view, seek_pos(pos));
                view_set_cursor_and_preferred_x(app, view, seek_pos(pos + (i32)string.size));
                
                ARGB_Color argb = fcolor_resolve(fcolor_id(defcolor_paste));
                buffer_post_fade(app, buffer, 0.667f, Ii64_size(pos, string.size), argb);
            }
        }
    }
}

CUSTOM_COMMAND_SIG(replace_paste_next)
CUSTOM_DOC("If the previous command was paste or paste_next, replaces the paste range with the next text down on the clipboard, otherwise operates as the paste command.")
{
    Scratch_Block scratch(app);
    
    i32 count = clipboard_count(0);
    if (count > 0){
        View_ID view = get_active_view(app, Access_ReadWriteVisible);
        Managed_Scope scope = view_get_managed_scope(app, view);
        no_mark_snap_to_cursor(app, scope);
        
        Rewrite_Type *rewrite = scope_attachment(app, scope, view_rewrite_loc, Rewrite_Type);
        if (rewrite != 0){
            if (*rewrite == Rewrite_Paste){
                Rewrite_Type *next_rewrite = scope_attachment(app, scope, view_next_rewrite_loc, Rewrite_Type);
                *next_rewrite = Rewrite_Paste;
                
                i32 *paste_index_ptr = scope_attachment(app, scope, view_paste_index_loc, i32);
                i32 paste_index = (*paste_index_ptr) + 1;
                *paste_index_ptr = paste_index;
                
                undo(app);
                String_Const_u8 string = push_clipboard_index(scratch, 0, paste_index);
                if (string.size > 0){
                    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWriteVisible);
                    
                    Range_i64 range = get_view_range(app, view);
                    i64 pos = range.min;
                    
                    buffer_replace_range(app, buffer, Ii64(pos, pos + string.size), string);
                    view_set_mark(app, view, seek_pos(pos));
                    view_set_cursor_and_preferred_x(app, view, seek_pos(pos + (i32)string.size));
                    
                    ARGB_Color argb = fcolor_resolve(fcolor_id(defcolor_paste));
                    buffer_post_fade(app, buffer, 0.667f, Ii64_size(pos, string.size), argb);
                }
            }
            else{
                replace_paste(app);
            }
        }
    }
}

CUSTOM_COMMAND_SIG(replace_handle_input)
CUSTOM_DOC("Handles input for the replace mode."){
    replace_write_char(app);
    move_right(app);
}


//
// Mode Replace Chord
//
CUSTOM_COMMAND_SIG(modal_set_chord_replace)
CUSTOM_DOC("Activates 'replace' chord.") {
    if(mod_active_buffer_has_write_access(app))
        modal_set_active_mapid(app, chord_replace);
}

function void modal_bind_chord_replace(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(chord_replace);
    ParentMap(mapid_vital);
    
    Bind(modal_revert_mode, KeyCode_Insert);
    
    Bind(modal_set_mode_normal, KeyCode_Escape);
    BindTextInput(replace_chord_handle_input);
}

CUSTOM_COMMAND_SIG(modal_set_theme_chord_replace)
CUSTOM_DOC("Sets the theme for 'replace' chord.") {
    MODAL_THEME_SET_COLOR(margin,        0, 0xff032459);
    MODAL_THEME_SET_COLOR(margin_hover,  0, 0xff1c338e);
    MODAL_THEME_SET_COLOR(margin_active, 0, 0xff1e439e);
    MODAL_THEME_COPY_COLOR(bar, 0, margin, 0);
    
    MODAL_THEME_SET_COLOR(cursor, 0, 0xffe91e63);
    MODAL_THEME_SET_COLOR(mark,   0, 0xffc30c4a);
}

CUSTOM_COMMAND_SIG(replace_chord_handle_input)
CUSTOM_DOC("Handles input for the replace chord mode."){
    replace_write_char(app);
    modal_revert_mode(app);
}


#endif // _MOD_REPLACE_CPP