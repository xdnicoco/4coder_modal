#if !defined(_MOD_PREFERENCES)
#define _MOD_PREFERENCES

CUSTOM_COMMAND_SIG(modal_set_mode_preferences)
CUSTOM_DOC("Activates 'preferences' mode.") {
    modal_set_active_mapid(app, chord_preferences);
    mod_show_bindings_hud(app);
}

function void modal_bind_chord_preferences(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(chord_preferences);
    ParentMap(mapid_vital);
    
    Bind(modal_set_mode_normal, KeyCode_Escape);
    Bind(modal_set_mode_normal, KeyCode_Return);
    
    Bind(preferences_toggle_filebar,            KeyCode_B);
    Bind(preferences_toggle_fullscreen,         KeyCode_F);
    Bind(preferences_toggle_line_wrap,          KeyCode_L);
    Bind(preferences_toggle_line_numbers,       KeyCode_N);
    Bind(preferences_toggle_mouse,              KeyCode_M);
    Bind(preferences_toggle_virtual_whitespace, KeyCode_V);
    Bind(preferences_toggle_show_whitespace,    KeyCode_V, KeyCode_Shift);
    
    Bind(preferences_invert_theme_colors, KeyCode_I);
    Bind(preferences_theme_lister,        KeyCode_T);
    
    Bind(preferences_reopen_file,  KeyCode_R);
    Bind(preferences_clean_lines,  KeyCode_C);
    Bind(preferences_load_project, KeyCode_P);
    
    Bind(preferences_reset_face_size, KeyCode_Equal);
    Bind(increase_face_size,          KeyCode_Equal, KeyCode_Shift);
    Bind(decrease_face_size,          KeyCode_Minus);
}

CUSTOM_COMMAND_SIG(modal_set_theme_chord_preferences)
CUSTOM_DOC("Sets the theme for 'preferences' mode.") {
    MODAL_THEME_SET_COLOR(back,          0, 0xff000000);
    MODAL_THEME_SET_COLOR(margin,        0, 0xff000000);
    MODAL_THEME_SET_COLOR(margin_hover,  0, 0xff041010);
    MODAL_THEME_SET_COLOR(margin_active, 0, 0xff082020);
    MODAL_THEME_COPY_COLOR(bar, 0, margin, 0);
    
    MODAL_THEME_SET_COLOR(cursor, 0, 0xff666666);
    MODAL_THEME_SET_COLOR(mark,   0, 0xff343434);
    MODAL_THEME_SET_COLOR(pop1,   0, 0xff55cccc);
    MODAL_THEME_SET_COLOR(pop2,   0, 0xffff5f5f);
}

CUSTOM_COMMAND_SIG(preferences_toggle_filebar)
CUSTOM_DOC("Toggles the visibility status of the current view's filebar."){
    toggle_filebar(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_toggle_fullscreen)
CUSTOM_DOC("Toggles the visibility status of the current view's fullscreen."){
    toggle_fullscreen(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_toggle_line_wrap)
CUSTOM_DOC("Toggles the visibility status of the current view's line wrap."){
    toggle_line_wrap(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_toggle_line_numbers)
CUSTOM_DOC("Toggles the visibility status of the current view's line wrap."){
    toggle_line_numbers(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_toggle_mouse)
CUSTOM_DOC("Toggles the mouse suppression mode, see suppress_mouse and allow_mouse."){
    toggle_mouse(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_toggle_virtual_whitespace)
CUSTOM_DOC("Toggles the visibility status of the current view's virtual whitespace."){
    toggle_virtual_whitespace(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_toggle_show_whitespace)
CUSTOM_DOC("Toggles the visibility status of the current view's whitespace."){
    toggle_show_whitespace(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_invert_theme_colors)
CUSTOM_DOC("Inverts the modal theme colors.") {
    modal_inverted_theme_colors = !modal_inverted_theme_colors;
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_theme_lister)
CUSTOM_DOC("Opens the 4coder color tweaker."){
    theme_lister(app);
    modal_revert_mode(app);
}


CUSTOM_COMMAND_SIG(preferences_reopen_file)
CUSTOM_DOC("Reloads the file opened in the active buffer."){
    reopen(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_clean_lines)
CUSTOM_DOC("Removes trailing whitespace from all lines in the current buffer."){
    clean_all_lines(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_load_project)
CUSTOM_DOC("See load_project."){
    load_project(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(preferences_reset_face_size)
CUSTOM_DOC("Increase the size of the face used by the current buffer.")
{
    View_ID view = get_active_view(app, Access_Always);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Always);
    Face_ID face_id = get_face_id(app, buffer);
    Face_Description description = get_face_description(app, face_id);
    description.parameters.pt_size = (u32)def_get_config_u64(app, vars_save_string_lit("default_font_size"));
    
    try_modify_face(app, face_id, &description);
}

#endif // _MOD_PREFERENCES