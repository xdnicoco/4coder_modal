# 4coder Modal Customization Layer
This is my EXPEREMENTAL(!!) customization layer of 4coder super in the journey to a modal future.
I took some elements from emacs, and some elements from vim, and added my own functions and functionality to create a highly customized modal system. Every mode has custom colors to indicate it is activated.

Updated to 4coder 4.1.8.

### Major modes:
* Normal  mode: Home mode for all the editing commands, the "home" that starts with 4coder.
* Insert  mode: Text insertion mode, remaps all the keys to writing their respective characters.
* Replace mode: Text replacement mode, replaces the text under the cursor. Mostly buggy.
### Features at work:
* [x] Keyboard macros
* [X] Modal api
* [ ] Snippets
* [ ] And so on...

# Installation
>**WARNING: This is my private custom layer, it is not supported by Allen nor by me, and it's purpose is to be educational and to serve as an example. Also, it is not compatible with OS X.**

[Support Allen Webster to get a copy of 4coder](http://www.4coder.com), clone the repository into your 4coder/custom directory and run build.bat from it.
For further information and documentaion check the [4coder documentaion page](http://4coder.net/).
