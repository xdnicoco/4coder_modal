#if !defined(_MODAL_COMMANDS_CPP)
#define _MODAL_COMMANDS_CPP

function Range_i64 mod_get_token_range_under_cursor(Application_Links *app, Scan_Direction direction) {
    Scratch_Block scratch(app);
    
    View_ID view = get_active_view(app, Access_Read);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Read);
    Boundary_Function_List boundaries = push_boundary_list(scratch, boundary_token, boundary_non_whitespace);
    i64 pos = view_get_cursor_pos(app, view);
    
    Range_i64 range = get_snipe_range(app, boundaries, buffer, pos, direction);
    
    return range;
}

function void query_start(Application_Links *app, Query_State *query, Command_Map_ID mapid=mode_query, b32 on_token=false) {
    modal_set_active_mapid(app, mapid);
    
    active_query_state = query;
    query->view = get_active_view(app, Access_ReadVisible);
    query->buffer = view_get_buffer(app, query->view, Access_ReadVisible);
    query->paste_index = 0;
    query->completion_pos = 0;
    query->selection.min = 0;
    query->selection.max = 0;
    query->stage = 0;
    
    if(on_token) {
        Range_i64 range = mod_get_token_range_under_cursor(app, Scan_Forward);
        range = range_clamp_size(range, MOD_QUERY_CAP);
        buffer_read_range(app, query->buffer, range, query->string[0].str);
        query->string[0].size = range_size(range);
    } else {
        query->string[0].size = 0;
    }
}

function void
start_search_like_query(Application_Links *app, String_Const_u8 prompt,
                        Custom_Command_Function apply_hook=0,
                        Custom_Command_Function update_hook=0,
                        Custom_Command_Function cancel_hook=0) {
    Query_State *query = &global_search_state.query;
    query_start(app, query);
    
    modal_set_theme(app, mode_search);
    global_search_state.do_highlights = true;
    
    query->prompt[0]   = prompt;
    query->apply_hook  = apply_hook;
    query->update_hook = update_hook;
    query->cancel_hook = cancel_hook;
}

function i64 get_word_start(String_Const_u8 string, i64 pos) {
    if(pos > (i64)string.size) pos = (i64)string.size;
    
    u8 *c = string.str + pos - 1;
    while(pos > 0 && (!character_is_alpha_numeric_unicode(*c) || (*c == '_'))) {
        --pos;
        --c;
    }
    while(pos > 0 &&  (character_is_alpha_numeric_unicode(*c) && (*c != '_'))) {
        --pos;
        --c;
    }
    
    return(pos);
}

function i64 get_next_word_start(String_Const_u8 string, i64 pos) {
    if(pos > (i64)string.size) pos = (i64)string.size;
    
    u8 *c = string.str + pos;
    while(pos < (i64)string.size && (!character_is_alpha_numeric_unicode(*c) || (*c == '_'))) {
        ++pos;
        ++c;
    }
    while(pos < (i64)string.size &&  (character_is_alpha_numeric_unicode(*c) && (*c != '_'))) {
        ++pos;
        ++c;
    }
    
    return(pos);
}

function void mod_describe_meta(Application_Links *app, Command_Metadata *meta) {
    Scratch_Block scratch(app);
    
    String_Const_u8 command_name = SCu8(meta->name, meta->name_len);
    Doc_Cluster *docs = doc_commands(scratch);
    Doc_Page *page = doc_get_page(docs, command_name);
    if(page) {
        Buffer_ID doc_buffer = render_doc_page(app, page);
        
        String_Const_u8 source_name = SCu8(meta->source_name, meta->source_name_len);
        String_Const_u8 command_location =
            push_u8_stringf(scratch, "%.*s:%d:1: %.*s\n", string_expand(source_name), meta->line_number, meta->name_len, meta->name);
        
        buffer_replace_range(app, doc_buffer, Ii64(), command_location);
        View_ID doc_view = get_or_open_build_panel(app);
        view_set_buffer(app, doc_view, doc_buffer, Access_Read);
    }
}

CUSTOM_COMMAND_SIG(mod_describe_binding)
CUSTOM_DOC("Shows a given key binding descriptions.") {
    View_ID view = get_this_ctx_view(app, Access_Always);
    if(!view_exists(app, view)) return;
    
    Query_Bar_Group group(app);
    
    Query_Bar bar = {};
    bar.prompt = string_u8_litexpr("Press a key combination");
    
    start_query_bar(app, &bar, 0);
    
    Command_Metadata *meta = 0;
    
    for(;;) {
        User_Input input = get_next_input(app, EventPropertyGroup_AnyUserInput, EventProperty_Escape);
        if(input.abort) break;
        
        Command_Binding binding = map_get_binding_recursive(&framework_mapping, global_active_mapid, &input.event);
        meta = get_command_metadata(binding.custom);
        if(meta) break;
    }
    
    if(meta) {
        mod_describe_meta(app, meta);
    }
}

CUSTOM_COMMAND_SIG(mod_toggler)
CUSTOM_DOC("Replaces the character under the cursor with a complementory character, for example replaces 0 with 1, replaces . with ->, replaces lowercase withh uppercase and vice versa etc."){
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    i64 pos = view_get_cursor_pos(app, view);
    
#define TOGGLER_GET_CHAR(offset) buffer_get_char(app, buffer, pos+(offset))
#define TOGGLER_REPLACE_RANGE(min, max, string) buffer_replace_range(app, buffer, Ii64(pos+(min), pos+(max)), string_u8_litexpr((string)))
    
    char c = TOGGLER_GET_CHAR(0);
    switch(c){
        case '-': {
            if(TOGGLER_GET_CHAR(1) == '-')       TOGGLER_REPLACE_RANGE(0,  2, "++");
            else if(TOGGLER_GET_CHAR(1) == '-')  TOGGLER_REPLACE_RANGE(-1, 1, "++");
            else if(TOGGLER_GET_CHAR(1) == '>')  TOGGLER_REPLACE_RANGE(0,  2, ".");
            else                                 TOGGLER_REPLACE_RANGE(0,  1, "+");
        } break;
        
        case '+': {
            if(TOGGLER_GET_CHAR(1) == '+')       TOGGLER_REPLACE_RANGE(0,  2, "--");
            else if(TOGGLER_GET_CHAR(-1) == '+') TOGGLER_REPLACE_RANGE(-1, 1, "--");
            else                                 TOGGLER_REPLACE_RANGE(0,  1, "-");
        } break;
        
        case '=': {
            if(TOGGLER_GET_CHAR(1) == '=')       TOGGLER_REPLACE_RANGE(0,  2, "!=");
            else if(TOGGLER_GET_CHAR(-1) == '=') TOGGLER_REPLACE_RANGE(-1, 1, "!=");
            else if(TOGGLER_GET_CHAR(-1) == '!') TOGGLER_REPLACE_RANGE(-1, 1, "==");
            else if(TOGGLER_GET_CHAR(-1) == '-') TOGGLER_REPLACE_RANGE(-1, 1, "+=");
            else if(TOGGLER_GET_CHAR(-1) == '+') TOGGLER_REPLACE_RANGE(-1, 1, "-=");
            else if(TOGGLER_GET_CHAR(-1) == '<') {
                if(TOGGLER_GET_CHAR(-2) == '<')  TOGGLER_REPLACE_RANGE(-2, 1, ">>=");
                else                             TOGGLER_REPLACE_RANGE(-1, 1, ">=");
            } else if(TOGGLER_GET_CHAR(-1) == '>') {
                if(TOGGLER_GET_CHAR(-2) == '>')  TOGGLER_REPLACE_RANGE(-2, 1, "<<=");
                else                             TOGGLER_REPLACE_RANGE(-1, 1, "<=");
            }
        } break;
        
        case '!': {
            if(TOGGLER_GET_CHAR(1) == '=')       TOGGLER_REPLACE_RANGE(0,  2, "==");
        } break;
        
        case '<': {
            if(TOGGLER_GET_CHAR(-1) == '<')      TOGGLER_REPLACE_RANGE(-1, 1, ">>");
            else if(TOGGLER_GET_CHAR(1) == '<')  TOGGLER_REPLACE_RANGE(0,  2, ">>");
            else if(TOGGLER_GET_CHAR(1) == '=')  TOGGLER_REPLACE_RANGE(0,  2, ">=");
            else                                 TOGGLER_REPLACE_RANGE(0,  1, ">");
        } break;
        
        case '>': {
            if(TOGGLER_GET_CHAR(-1) == '-')      TOGGLER_REPLACE_RANGE(-1, 1, ".");
            else if(TOGGLER_GET_CHAR(-1) == '>') TOGGLER_REPLACE_RANGE(-1, 1, "<<");
            else if(TOGGLER_GET_CHAR(1) == '>')  TOGGLER_REPLACE_RANGE(0,  2, "<<");
            else if(TOGGLER_GET_CHAR(1) == '=')  TOGGLER_REPLACE_RANGE(0,  2, "<=");
            else                                 TOGGLER_REPLACE_RANGE(0,  1, "<");
        } break;
        
        case '|': {
            if(TOGGLER_GET_CHAR(-1) == '|')      TOGGLER_REPLACE_RANGE(-1, 1, "&&");
            else if(TOGGLER_GET_CHAR(1) == '|')  TOGGLER_REPLACE_RANGE(0,  2, "&&");
            else                                 TOGGLER_REPLACE_RANGE(0,  1, "&");
        } break;
        
        case '&': {
            if(TOGGLER_GET_CHAR(-1) == '&')      TOGGLER_REPLACE_RANGE(-1, 1, "||");
            else if(TOGGLER_GET_CHAR(1) == '&')  TOGGLER_REPLACE_RANGE(0,  2, "||");
            else                                 TOGGLER_REPLACE_RANGE(0,  1, "|");
        } break;
        
        case '.':  { TOGGLER_REPLACE_RANGE(0, 1, "->"); } break;
        case '0':  { TOGGLER_REPLACE_RANGE(0, 1, "1");  } break;
        case '1':  { TOGGLER_REPLACE_RANGE(0, 1, "0");  } break;
        case '\\': { TOGGLER_REPLACE_RANGE(0, 1, "/");  } break;
        case '/':  { TOGGLER_REPLACE_RANGE(0, 1, "\\"); } break;
        case ' ':  { TOGGLER_REPLACE_RANGE(0, 1, "_");  } break;
        case '_':  { TOGGLER_REPLACE_RANGE(0, 1, " ");  } break;
        
        default: {
            if(character_is_upper(c))      c = character_to_lower(c);
            else if(character_is_lower(c)) c = character_to_upper(c);
            
            buffer_replace_range(app, buffer, Ii64(pos, pos + 1), SCu8(&c, 1));
        } break;
    }
    
#undef TOGGLER_REPLACE_RANGE
#undef TOGGLER_GET_CHAR
}

void mod_rectangular_replace_proc(Application_Links *app, String_Const_u8 str){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    History_Group history_group = history_group_begin(app, buffer);
    
    b32 defer_enable_virtual_whitespace = def_get_config_b32(vars_save_string_lit("defer_enable_virtual_whitespace"));
    if (defer_enable_virtual_whitespace) {
        toggle_virtual_whitespace(app);
        auto_indent_range(app);
    }
    
    Range_i64 range = get_view_range(app, view);
    
    Buffer_Cursor cursor = buffer_compute_cursor(app, buffer, seek_pos(range.min));
    Buffer_Cursor mark = buffer_compute_cursor(app, buffer, seek_pos(range.max));
    i64 min_line = mark.line;
    i64 max_line = cursor.line;
    if(min_line > max_line) Swap(i64, min_line, max_line);
    
    i64 min_col = mark.col;
    i64 max_col = cursor.col;
    if(min_col > max_col)   Swap(i64, min_col, max_col);
    
    i64 line_count = max_line - min_line + 1;
    
    Batch_Edit *first = 0;
    Batch_Edit *last = 0;
    
    for(i32 i = 0; i < line_count; ++i) {
        Buffer_Seek min_seek = seek_line_col(min_line + i, min_col);
        Buffer_Seek max_seek = seek_line_col(min_line + i, max_col);
        
        Buffer_Cursor min_cursor = buffer_compute_cursor(app, buffer, min_seek);
        Buffer_Cursor max_cursor = buffer_compute_cursor(app, buffer, max_seek);
        if(min_cursor.line == min_line + i) {
            Batch_Edit *edit = push_array(scratch, Batch_Edit, 1);
            sll_queue_push(first, last, edit);
            edit->edit.text = str;
            edit->edit.range = Ii64(min_cursor.pos, max_cursor.pos);
        }
    }
    
    if(first) buffer_batch_edit(app, buffer, first);
    if(defer_enable_virtual_whitespace) toggle_virtual_whitespace(app);
    history_group_end(history_group);
}

void mod_rectangular_fill_proc(Application_Links *app, u8 c){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    History_Group history_group = history_group_begin(app, buffer);
    
    b32 defer_enable_virtual_whitespace = def_get_config_b32(vars_save_string_lit("defer_enable_virtual_whitespace"));
    if (defer_enable_virtual_whitespace) {
        toggle_virtual_whitespace(app);
        auto_indent_range(app);
    }
    
    Buffer_Cursor cursor = buffer_compute_cursor(app, buffer, seek_pos(view_get_cursor_pos(app, view)));
    Buffer_Cursor mark   = buffer_compute_cursor(app, buffer, seek_pos(view_get_mark_pos(app, view)));
    
    i64 min_line = mark.line, max_line = cursor.line;
    if(min_line > max_line) Swap(i64, min_line, max_line);
    
    i64 min_col = mark.col, max_col = cursor.col;
    if(min_col > max_col) Swap(i64, min_col, max_col);
    
    i64 line_count = max_line - min_line + 1;
    i64 char_count = max_col - min_col;
    
    Batch_Edit *first = 0;
    Batch_Edit *last = 0;
    
    u8 *str_space = push_array(scratch, u8, char_count);
    block_fill_u8(str_space, char_count, c);
    String_Const_u8 str = SCu8(str_space, char_count);
    
    for(i32 i = 0; i < line_count; ++i) {
        Buffer_Seek min_seek = seek_line_col(min_line + i, min_col);
        Buffer_Seek max_seek = seek_line_col(min_line + i, max_col);
        
        Buffer_Cursor min_cursor = buffer_compute_cursor(app, buffer, min_seek);
        Buffer_Cursor max_cursor = buffer_compute_cursor(app, buffer, max_seek);
        if(min_cursor.line == min_line + i) {
            Batch_Edit *edit = push_array(scratch, Batch_Edit, 1);
            sll_queue_push(first, last, edit);
            edit->edit.text = str;
            edit->edit.range = Ii64(min_cursor.pos, max_cursor.pos);
        }
    }
    
    if(first) buffer_batch_edit(app, buffer, first);
    if(defer_enable_virtual_whitespace) toggle_virtual_whitespace(app);
    history_group_end(history_group);
}

CUSTOM_COMMAND_SIG(mod_rectangular_delete)
CUSTOM_DOC("Deletes the range in a rectangular fashion."){
    mod_rectangular_replace_proc(app, {});
}

CUSTOM_COMMAND_SIG(mod_rectangular_replace)
CUSTOM_DOC("Replaces the range in a rectangular fashion."){
    b32 in_query = modal_active_mode_inherits_from(app, mode_query);
    
    if(!in_query) {
        start_search_like_query(app, string_u8_litexpr("With: "), mod_rectangular_replace);
        global_search_state.do_highlights = false;
    } else {
        Query_State *query = active_query_state;
        String_Const_u8 response = SCu8(query->string[0]);
        if (response.size > 0) {
            mod_rectangular_replace_proc(app, response);
        }
    }
}

CUSTOM_COMMAND_SIG(mod_rectangular_fill)
CUSTOM_DOC("Fills the range in a rectangular fashion."){
    u8 fill_char;
    String_Const_u8 response = get_query_string(app, "With: ", &fill_char, 1);
    if (response.size > 0) {
        mod_rectangular_fill_proc(app, fill_char);
    }
}

CUSTOM_COMMAND_SIG(mod_rectangular_fill_spaces)
CUSTOM_DOC("Fills the range in a rectangular fashion."){
    mod_rectangular_fill_proc(app, ' ');
}

CUSTOM_COMMAND_SIG(mod_rectangular_copy)
CUSTOM_DOC("Copys in a rectangular fashion."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_Read);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Read);
    History_Group history_group = history_group_begin(app, buffer);
    
    b32 defer_enable_virtual_whitespace = def_get_config_b32(vars_save_string_lit("defer_enable_virtual_whitespace"));
    if (defer_enable_virtual_whitespace) {
        toggle_virtual_whitespace(app);
        auto_indent_range(app);
    }
    
    Buffer_Cursor cursor = buffer_compute_cursor(app, buffer, seek_pos(view_get_cursor_pos(app, view)));
    Buffer_Cursor mark   = buffer_compute_cursor(app, buffer, seek_pos(view_get_mark_pos(app, view)));
    
    i64 min_line = mark.line, max_line = cursor.line;
    if(min_line > max_line) Swap(i64, min_line, max_line);
    
    i64 min_col = mark.col, max_col = cursor.col;
    if(min_col > max_col) Swap(i64, min_col, max_col);
    
    i64 line_count = max_line - min_line + 1;
    
    List_String_Const_u8 lines = {};
    for(i32 i = 0; i < line_count; ++i) {
        Buffer_Seek min_seek = seek_line_col(min_line + i, min_col);
        Buffer_Seek max_seek = seek_line_col(min_line + i, max_col);
        Buffer_Cursor min_cursor = buffer_compute_cursor(app, buffer, min_seek);
        Buffer_Cursor max_cursor = buffer_compute_cursor(app, buffer, max_seek);
        
        String_Const_u8 line = push_buffer_range(app, scratch, buffer, Ii64(min_cursor.pos, max_cursor.pos));
        string_list_push(scratch, &lines, line);
    }
    
    String_Const_u8 copy_data = string_list_flatten(scratch, lines, 0, SCu8("\n"), 0, StringFill_NoTerminate);
    if(copy_data.size > 0) clipboard_post(app, 0, copy_data);
    if(defer_enable_virtual_whitespace) toggle_virtual_whitespace(app);
    history_group_end(history_group);
}


CUSTOM_COMMAND_SIG(mod_rectangular_cut)
CUSTOM_DOC("Cuts in a rectangular fashion."){
    mod_rectangular_copy(app);
    mod_rectangular_delete(app);
}

CUSTOM_COMMAND_SIG(mod_rectangular_paste)
CUSTOM_DOC("Pastes in a rectangular fashion."){
    clipboard_update_history_from_system(app, 0);
    i32 count = clipboard_count(0);
    if (count > 0){
        View_ID view = get_active_view(app, Access_ReadWriteVisible);
        Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWriteVisible);
        History_Group history_group = history_group_begin(app, buffer);
        
        Managed_Scope scope = view_get_managed_scope(app, view);
        Rewrite_Type *next_rewrite = scope_attachment(app, scope, view_next_rewrite_loc, Rewrite_Type);
        if (next_rewrite != 0){
            b32 defer_enable_virtual_whitespace = def_get_config_b32(vars_save_string_lit("defer_enable_virtual_whitespace"));
            if (defer_enable_virtual_whitespace) {
                toggle_virtual_whitespace(app);
                auto_indent_range(app);
            }
            
            *next_rewrite = Rewrite_Paste;
            i32 *paste_index = scope_attachment(app, scope, view_paste_index_loc, i32);
            *paste_index = 0;
            
            Scratch_Block scratch(app);
            
            Buffer_Cursor cursor = buffer_compute_cursor(app, buffer, seek_pos(view_get_cursor_pos(app, view)));
            
            Batch_Edit *first = 0;
            Batch_Edit *last  = 0;
            
            String_Const_u8 string = push_clipboard_index(scratch, 0, *paste_index);
            List_String_Const_u8 splits = string_split(scratch, string, (u8*)"\n", 1);
            Node_String_Const_u8 *line = splits.first;
            i64 line_index = 0;
            i64 fade_offset = 0;
            while(line) {
                if(line->string.size > 0) {
                    Batch_Edit *edit = push_array(scratch, Batch_Edit, 1);
                    sll_queue_push(first, last, edit);
                    
                    Buffer_Cursor line_cursor =
                        buffer_compute_cursor(app, buffer, seek_line_col(cursor.line + line_index, cursor.col));
                    
                    edit->edit.range = Ii64(line_cursor.pos, line_cursor.pos);
                    edit->edit.text = line->string;
                    
                    ARGB_Color argb = fcolor_resolve(fcolor_id(defcolor_paste));
                    buffer_post_fade(app, buffer, 0.667f, Ii64_size(line_cursor.pos + fade_offset, line->string.size), argb);
                    fade_offset += line->string.size;
                }
                line = line->next;
                ++line_index;
            }
            
            buffer_batch_edit(app, buffer, first);
            if(defer_enable_virtual_whitespace) toggle_virtual_whitespace(app);
            history_group_end(history_group);
        }
    }
}

CUSTOM_COMMAND_SIG(mod_rectangular_paste_repeated)
CUSTOM_DOC("Pastes in a repeated rectangular fashion."){
    clipboard_update_history_from_system(app, 0);
    i32 count = clipboard_count(0);
    if (count > 0){
        View_ID view = get_active_view(app, Access_ReadWriteVisible);
        Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWriteVisible);
        History_Group history_group = history_group_begin(app, buffer);
        
        Managed_Scope scope = view_get_managed_scope(app, view);
        Rewrite_Type *next_rewrite = scope_attachment(app, scope, view_next_rewrite_loc, Rewrite_Type);
        if (next_rewrite != 0){
            b32 defer_enable_virtual_whitespace = def_get_config_b32(vars_save_string_lit("defer_enable_virtual_whitespace"));
            if (defer_enable_virtual_whitespace) {
                toggle_virtual_whitespace(app);
                auto_indent_range(app);
            }
            
            *next_rewrite = Rewrite_Paste;
            i32 *paste_index = scope_attachment(app, scope, view_paste_index_loc, i32);
            *paste_index = 0;
            
            Scratch_Block scratch(app);
            
            Buffer_Cursor cursor = buffer_compute_cursor(app, buffer, seek_pos(view_get_cursor_pos(app, view)));
            Buffer_Cursor mark   = buffer_compute_cursor(app, buffer, seek_pos(view_get_mark_pos(app, view)));
            
            i64 min_line = mark.line, max_line = cursor.line;
            if(min_line > max_line) Swap(i64, min_line, max_line);
            i64 line_count = max_line - min_line + 1;
            
            Batch_Edit *first = 0;
            Batch_Edit *last  = 0;
            
            String_Const_u8 string = push_clipboard_index(scratch, 0, *paste_index);
            List_String_Const_u8 splits = string_split(scratch, string, (u8*)"\n", 1);
            
            i64 line_index = 0;
            while(line_index < line_count) {
                Node_String_Const_u8 *line = splits.first;
                while(line) {
                    if(line->string.size > 0) {
                        Buffer_Cursor line_cursor =
                            buffer_compute_cursor(app, buffer, seek_line_col(min_line + line_index, cursor.col));
                        
                        Batch_Edit *edit = push_array(scratch, Batch_Edit, 1);
                        edit->edit.range = Ii64(line_cursor.pos);
                        edit->edit.text = line->string;
                        
                        sll_queue_push(first, last, edit);
                    }
                    line = line->next;
                    ++line_index;
                    if(line_index >= line_count) break;
                }
            }
            
            buffer_batch_edit(app, buffer, first);
            if(defer_enable_virtual_whitespace) toggle_virtual_whitespace(app);
            history_group_end(history_group);
        }
    }
}

CUSTOM_COMMAND_SIG(mod_clean_redundant_whitespace)
CUSTOM_DOC("Deletes all spaces that are concecutive in the range."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    Range_i64 range = get_view_range(app, view);
    String_Const_u8 source = push_buffer_range(app, scratch, buffer, range);
    String_u8 string = string_u8_push(scratch, source.size);
    string.size = 0;
    
    for(u64 i = 0; i < source.size-1; ++i) {
        if(source.str[i] == source.str[i+1] && source.str[i] != '\n' &&
           character_is_whitespace(source.str[i])) {
            // do nothing
        } else {
            string_append_character(&string, source.str[i]);
        }
    }
    string_append_character(&string, source.str[source.size-1]);
    
    buffer_replace_range(app, buffer, range, SCu8(string));
    view_set_mark(app, view, seek_pos(range.min));
    view_set_cursor(app, view, seek_pos(range.min + string.size));
}

i64 mod_ocd_proc_aligner(Application_Links *app, Buffer_ID buffer, Range_i64 range, String_Const_u8 needle,
                         i64 starting_col, char fill_char, b32 include_separator) {
    Scratch_Block scratch(app);
    i64 current_line = 0;
    i64 max_col = 0, min_col = INT64_MAX;
    
    Batch_Edit *first = 0;
    Batch_Edit *last = 0;
    
    String_Match_List matches =
        buffer_find_all_matches(app, scratch, buffer, 0, range, needle,
                                &character_predicate_alpha_numeric_underscore_utf8, Scan_Forward);
    string_match_list_filter_flags(&matches, StringMatch_CaseSensitive, 0);
    
    b32 should_edit = false;
    for(String_Match *match = matches.first; match; match = match->next) {
        Buffer_Cursor cursor;
        if(include_separator) {
            cursor = buffer_compute_cursor(app, buffer, seek_pos(match->range.min));
        } else {
            cursor = buffer_compute_cursor(app, buffer, seek_pos(match->range.max));
        }
        
        i64 line_end = get_line_end_pos(app, buffer, cursor.line);
        if((cursor.line != current_line) && (cursor.col > starting_col) && (cursor.pos < line_end)) {
            Batch_Edit *edit = push_array(scratch, Batch_Edit, 1);
            sll_queue_push(first, last, edit);
            edit->edit.text = {};
            edit->edit.range = {cursor.line, cursor.col};
            
            if((max_col > 0) && (max_col != cursor.col)) {
                should_edit = true;
            }
            max_col = Max(max_col, cursor.col);
            min_col = Min(min_col, cursor.col);
            
            current_line = cursor.line;
        }
    }
    
    if(should_edit){
        i64 space_count = max_col - min_col + 1;
        u8 *spaces = push_array(scratch, u8, space_count);
        block_fill_u8(spaces, space_count, fill_char);
        String_Const_u8 edit_string = SCu8(spaces, space_count);
        Batch_Edit *edit = first;
        i64 actual_edit_count = 0;
        while(edit) {
            Buffer_Cursor cursor = buffer_compute_cursor(app, buffer, seek_line_col(edit->edit.range.min, edit->edit.range.max));
            i64 space_diff = max_col - cursor.col;
            edit->edit.range = Ii64(cursor.pos);
            edit->edit.text = string_prefix(edit_string, space_diff);
            if(edit->edit.text.size > 0) {
                actual_edit_count += 1;
            }
            edit = edit->next;
        }
        if(actual_edit_count > 0) {
            buffer_batch_edit(app, buffer, first);
        }
    }
    
    return max_col;
}

void mod_ocd_proc(Application_Links *app, String_Const_u8 needle, char fill_char, b32 include_separator){
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    History_Group history_group = history_group_begin(app, buffer);
    
    b32 defer_enable_virtual_whitespace = def_get_config_b32(vars_save_string_lit("defer_enable_virtual_whitespace"));
    if (defer_enable_virtual_whitespace) {
        toggle_virtual_whitespace(app);
        auto_indent_range(app);
    }
    
    i64 col, next_col = -1;
    do {
        col = next_col;
        Range_i64 range = get_view_range(app, view);
        next_col = mod_ocd_proc_aligner(app, buffer, range, needle, col, fill_char, include_separator);
        history_group_end(history_group);
        history_group = history_group_begin(app, buffer);
    } while(col < next_col);
    
    if (defer_enable_virtual_whitespace) toggle_virtual_whitespace(app);
    history_group_end(history_group);
}

CUSTOM_COMMAND_SIG(mod_ocd_after)
CUSTOM_DOC("Queries for an arbitrary character, then aligns the text after it to be on the same column for every line in the range."){
    String_Const_u8 response = get_query_string(app, "OCD after separator: ", mod_query_bar_space, MOD_QUERY_CAP);
    
    if (response.size > 0) {
        mod_ocd_proc(app, response, ' ', false);
    }
}

CUSTOM_COMMAND_SIG(mod_ocd_before)
CUSTOM_DOC("Queries for an arbitrary character, then aligns it to be on the same column for every line in the range."){
    String_Const_u8 response = get_query_string(app, "OCD before separator: ", mod_query_bar_space, MOD_QUERY_CAP);
    
    if (response.size > 0) {
        mod_ocd_proc(app, response, ' ', true);
    }
}

CUSTOM_COMMAND_SIG(mod_ocd_equals)
CUSTOM_DOC("Aligns the first '=' character to be on the same column of every line in the range."){
    mod_ocd_proc(app, string_u8_litexpr("="), ' ', true);
}

CUSTOM_COMMAND_SIG(mod_select_line)
CUSTOM_DOC("Selects the line under the cursor."){
    View_ID view = get_active_view(app, Access_Read);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Read);
    
    i64 pos = view_get_cursor_pos(app, view);
    i64 line = get_line_number_from_pos(app, buffer, pos);
    
    i64 offset = 0;
    String_ID key = vars_save_string_lit("enable_virtual_whitespace");
    b32 enable_virtual_whitespace = def_get_config_b32(key);
    if(enable_virtual_whitespace) {
        Scratch_Block scratch(app);
        String_Const_u8 line_str = push_buffer_line(app, scratch, buffer, line);
        offset = string_find_first_non_whitespace(line_str) + 1;
    }
    
    view_set_mark(app, view, seek_line_col(line, offset));
    view_set_cursor(app, view, seek_line_col(line + 1, 0));
}

CUSTOM_COMMAND_SIG(mod_backspace_line)
CUSTOM_DOC("Concatinates the current line with the one before it, adding a space between them."){
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    History_Group history_group = history_group_begin(app, buffer);
    seek_beginning_of_line(app);
    backspace_char(app);
    write_text(app, string_u8_litexpr(" "));
    history_group_end(history_group);
}

CUSTOM_COMMAND_SIG(mod_copy_line)
CUSTOM_DOC("Copys the line under the cursor."){
    mod_select_line(app);
    copy(app);
}

CUSTOM_COMMAND_SIG(mod_cut_line)
CUSTOM_DOC("Cuts the line under the cursor."){
    mod_select_line(app);
    cut(app);
}

CUSTOM_COMMAND_SIG(mod_select_token_forward)
CUSTOM_DOC("Selects a single, whole token on or to the left of the cursor."){
    View_ID view = get_active_view(app, Access_Read);
    
    Range_i64 range = mod_get_token_range_under_cursor(app, Scan_Forward);
    
    view_set_mark(app, view, seek_pos(range.start));
    view_set_cursor(app, view, seek_pos(range.end));
}

CUSTOM_COMMAND_SIG(mod_select_token_backward)
CUSTOM_DOC("Selects a single, whole token on or to the left of the cursor."){
    View_ID view = get_active_view(app, Access_Read);
    
    Range_i64 range = mod_get_token_range_under_cursor(app, Scan_Backward);
    
    view_set_mark(app, view, seek_pos(range.start));
    view_set_cursor(app, view, seek_pos(range.end));
}

CUSTOM_COMMAND_SIG(mod_copy_token)
CUSTOM_DOC("Copies a single, whole token on or to the left of the cursor."){
    mod_select_token_forward(app);
    copy(app);
}

CUSTOM_COMMAND_SIG(mod_cut_token)
CUSTOM_DOC("Cuts a single, whole token on or to the left of the cursor."){
    mod_select_token_forward(app);
    cut(app);
}

internal i64
mod_boundary_alpha_numeric_camel(Application_Links *app, Buffer_ID buffer, Side side, Scan_Direction direction, i64 pos){
    // HACK: might be better to create something like character_predicate_non_uppercase
    i64 result = boundary_alpha_numeric_camel(app, buffer, side, direction, pos);
    
    i32 offset = 0;
    switch(direction) {
        case Scan_Forward:  { offset = +1; } break;
        case Scan_Backward: { offset = -1; } break;
    }
    
    u8 c = buffer_get_char(app, buffer, result + offset);
    while(character_is_upper(c)) {
        result += offset;
        c = buffer_get_char(app, buffer, result + offset);
    }
    
    return(result);
}

CUSTOM_COMMAND_SIG(mod_move_right_alpha_numeric_or_camel_boundary)
CUSTOM_DOC("Customized version of move_right_alpha_numeric_or_camel_boundary.") {
    Scratch_Block scratch(app);
    current_view_scan_move(app, Scan_Forward, push_boundary_list(scratch, mod_boundary_alpha_numeric_camel));
}

CUSTOM_COMMAND_SIG(mod_move_left_alpha_numeric_or_camel_boundary)
CUSTOM_DOC("Customized version of move_left_alpha_numeric_or_camel_boundary.") {
    Scratch_Block scratch(app);
    current_view_scan_move(app, Scan_Backward, push_boundary_list(scratch, mod_boundary_alpha_numeric_camel));
}

CUSTOM_COMMAND_SIG(mod_backspace_word)
CUSTOM_DOC("Delete a single, whole token or camel case word on or to the left of the cursor and post it to the clipboard."){
    Scratch_Block scratch(app);
    current_view_boundary_delete(app, Scan_Backward,
                                 push_boundary_list(scratch, mod_boundary_alpha_numeric_camel));
}

CUSTOM_COMMAND_SIG(mod_delete_word)
CUSTOM_DOC("Delete a single, whole token or camel case word on or to the right of the cursor and post it to the clipboard."){
    Scratch_Block scratch(app);
    current_view_boundary_delete(app, Scan_Forward,
                                 push_boundary_list(scratch, mod_boundary_alpha_numeric_camel));
}

CUSTOM_COMMAND_SIG(mod_increment_digit_decimal)
CUSTOM_DOC("Increment the digit under the cursor, when arriving to 9 loops back to 0."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    i64 pos = view_get_cursor_pos(app, view);
    char c = buffer_get_char(app, buffer, pos);
    if(character_is_base10(c)){
        i64 digit = string_to_integer(SCchar(&c, (u64)1), 10) + 1;
        while(digit > 9) digit -= 10;
        buffer_replace_range(app, buffer, Ii64(pos, pos + 1), string_from_integer(scratch, digit, 10));
    }
}

CUSTOM_COMMAND_SIG(mod_decrement_digit_decimal)
CUSTOM_DOC("Decrement the digit under the cursor, when arriving to 0 loops back to 9."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    i64 pos = view_get_cursor_pos(app, view);
    char c = buffer_get_char(app, buffer, pos);
    if(character_is_base10(c)){
        i64 digit = string_to_integer(SCchar(&c, (u64)1), 10) - 1;
        while(digit < 0) digit += 10;
        buffer_replace_range(app, buffer, Ii64(pos, pos + 1), string_from_integer(scratch, digit, 10));
    }
}

CUSTOM_COMMAND_SIG(mod_increment_token_decimal)
CUSTOM_DOC("Increment the token under the cursor, when arriving to 9 loops back to 0."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    Range_i64 range = mod_get_token_range_under_cursor(app, Scan_Forward);
    
    b8 is_positive = (buffer_get_char(app, buffer, range.min) == '-');
    if(!is_positive) range.min += 1;
    
    String_Const_u8 token = push_buffer_range(app, scratch, buffer, range);
    
    if(string_is_integer(token, 10)){
        i64 number = string_to_integer(token, 10);
        if(!is_positive) number *= -1;
        number += 1;
        if(number < 0) number *= -1;
        
        buffer_replace_range(app, buffer, range, string_from_integer(scratch, number, 10));
    }
}

CUSTOM_COMMAND_SIG(mod_decrement_token_decimal)
CUSTOM_DOC("Decrement the token under the cursor, when arriving to 0 loops back to 9."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    Range_i64 range = mod_get_token_range_under_cursor(app, Scan_Forward);
    
    b8 is_positive = (buffer_get_char(app, buffer, range.min) == '-');
    if(!is_positive) range.min += 1;
    
    String_Const_u8 token = push_buffer_range(app, scratch, buffer, range);
    
    if(string_is_integer(token, 10)){
        i64 number = string_to_integer(token, 10);
        if(!is_positive) number *= -1;
        number -= 1;
        if(number < 0) number *= -1;
        
        buffer_replace_range(app, buffer, range, string_from_integer(scratch, number, 10));
    }
}

CUSTOM_COMMAND_SIG(mod_toggle_scratch)
CUSTOM_DOC("Sets the buffer in the active view to *scratch*."){
    View_ID view = get_active_view(app, Access_Always);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Always);
    Buffer_ID scratch_buffer = get_buffer_by_name(app, string_u8_litexpr("*scratch*"), Access_Always);
    
    if(buffer == scratch_buffer) buffer = get_buffer_next(app, 0, Access_Always);
    else buffer = scratch_buffer;
    
    view_set_buffer(app, view, buffer, 0);
}

CUSTOM_COMMAND_SIG(mod_toggle_search)
CUSTOM_DOC("Sets the buffer in the active view to *search*."){
    View_ID view = get_active_view(app, Access_Always);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Always);
    Buffer_ID search_buffer = get_buffer_by_name(app, string_u8_litexpr("*search*"), Access_Always);
    
    if(buffer == search_buffer) buffer = get_buffer_next(app, 0, Access_Always);
    else buffer = search_buffer;
    
    view_set_buffer(app, view, buffer, 0);
}

// @Cutnpaste from 4coder/custom/4coder_base_commands.cpp:1575:1: get_cpp_matching_file {
function b32
mod_get_matching_file(Application_Links *app, Buffer_ID buffer, Buffer_ID *buffer_out){
    b32 result = false;
    Scratch_Block scratch(app);
    String_Const_u8 file_name = push_buffer_file_name(app, scratch, buffer);
    if (file_name.size > 0){
        String_Const_u8 extension = string_file_extension(file_name);
        String_Const_u8 new_extensions[2] = {};
        i32 new_extensions_count = 0;
        if (string_match(extension, string_u8_litexpr("cpp")) || string_match(extension, string_u8_litexpr("cc"))){
            new_extensions[0] = string_u8_litexpr("h");
            new_extensions[1] = string_u8_litexpr("hpp");
            new_extensions_count = 2;
        }
        else if (string_match(extension, string_u8_litexpr("c"))){
            new_extensions[0] = string_u8_litexpr("h");
            new_extensions_count = 1;
        }
        else if (string_match(extension, string_u8_litexpr("h"))){
            new_extensions[0] = string_u8_litexpr("c");
            new_extensions[1] = string_u8_litexpr("cpp");
            new_extensions_count = 2;
        }
        else if (string_match(extension, string_u8_litexpr("cu"))){
            new_extensions[0] = string_u8_litexpr("cuh");
            new_extensions_count = 1;
        }
        else if (string_match(extension, string_u8_litexpr("cuh"))){
            new_extensions[0] = string_u8_litexpr("cu");
            new_extensions_count = 1;
        }
        else if (string_match(extension, string_u8_litexpr("hpp"))){
            new_extensions[0] = string_u8_litexpr("cpp");
            new_extensions_count = 1;
        }
        else if (string_match(extension, string_u8_litexpr("sh"))){
            new_extensions[0] = string_u8_litexpr("bat");
            new_extensions_count = 1;
        }
        else if (string_match(extension, string_u8_litexpr("bat"))){
            new_extensions[0] = string_u8_litexpr("sh");
            new_extensions_count = 1;
        }
        
        String_Const_u8 file_without_extension = string_file_without_extension(file_name);
        for (i32 i = 0; i < new_extensions_count; i += 1){
            Temp_Memory temp = begin_temp(scratch);
            String_Const_u8 new_extension = new_extensions[i];
            String_Const_u8 new_file_name = push_u8_stringf(scratch, "%.*s.%.*s", string_expand(file_without_extension), string_expand(new_extension));
            if (open_file(app, buffer_out, new_file_name, false, true)){
                result = true;
                break;
            }
            end_temp(temp);
        }
    }
    
    return(result);
}

CUSTOM_COMMAND_SIG(mod_open_matching_file)
CUSTOM_DOC("If the current file has a corresponding file type (i.e. *.cpp and *.h), attempts to open the corresponding file in the other view."){
    View_ID view = get_active_view(app, Access_Always);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Always);
    Buffer_ID new_buffer = 0;
    if (mod_get_matching_file(app, buffer, &new_buffer)){
        view = get_next_view_looped_primary_panels(app, view, Access_Always);
        view_set_buffer(app, view, new_buffer, 0);
        view_set_active(app, view);
    }
}

CUSTOM_COMMAND_SIG(mod_open_matching_file_same_panel)
CUSTOM_DOC("If the current file has a corresponding file type (i.e. *.cpp and *.h), attempts to open the corresponding file in the current view."){
    View_ID view = get_active_view(app, Access_Always);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Always);
    
    Buffer_ID target;
    if (mod_get_matching_file(app, buffer, &target)){
        view_set_buffer(app, view, target, 0);
    }
}

// }

void mod_surround_range_proc(Application_Links *app, String_Const_u8 prefix, String_Const_u8 postfix) {
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    Range_i64 range = get_view_range(app, view);
    
    Batch_Edit edits[2] = {};
    
    edits[0].edit.text  = prefix;
    edits[0].edit.range = Ii64(range.start);
    edits[0].next = &edits[1];
    
    edits[1].edit.text  = postfix;
    edits[1].edit.range = Ii64(range.end);
    
    buffer_batch_edit(app, buffer, edits);
}

CUSTOM_COMMAND_SIG(mod_surround_range)
CUSTOM_DOC("Surrounds the range with a prefix and a postfix."){
    u8 prefix_space[MOD_QUERY_CAP], postfix_space[MOD_QUERY_CAP];
    String_Const_u8 prefix  = get_query_string(app, "Prefix: ",  prefix_space,  MOD_QUERY_CAP);
    String_Const_u8 postfix = get_query_string(app, "Postfix: ", postfix_space, MOD_QUERY_CAP);
    mod_surround_range_proc(app, prefix, postfix);
}

CUSTOM_COMMAND_SIG(mod_parenthesize_range)
CUSTOM_DOC("Surrounds the range with '(' and ')'."){
    mod_surround_range_proc(app, string_u8_litexpr("("), string_u8_litexpr(")"));
}

CUSTOM_COMMAND_SIG(mod_quote_range)
CUSTOM_DOC("Surrounds the range with quotes."){
    mod_surround_range_proc(app, string_u8_litexpr("\""), string_u8_litexpr("\""));
}

CUSTOM_COMMAND_SIG(mod_comment_range)
CUSTOM_DOC("Surrounds the range with '/*' and '*/'."){
    mod_surround_range_proc(app, string_u8_litexpr("/*"), string_u8_litexpr("*/"));
}

CUSTOM_COMMAND_SIG(mod_append_semicolon_to_line)
CUSTOM_DOC("Inserts ';' at the end of the line under the cursor, and moves to the next line."){
    seek_end_of_textual_line(app);
    write_text(app, string_u8_litexpr(";"));
    move_down_textual(app);
}

CUSTOM_COMMAND_SIG(mod_append_backslash_to_line)
CUSTOM_DOC("Inserts '\\' at the end of the line under the cursor, and moves to the next line."){
    seek_end_of_textual_line(app);
    write_text(app, string_u8_litexpr(" \\"));
    move_down_textual(app);
}

CUSTOM_COMMAND_SIG(mod_write_arrow)
CUSTOM_DOC("Writes a '->'."){
    write_text(app, string_u8_litexpr("->"));
}

CUSTOM_COMMAND_SIG(mod_append_comma_to_line)
CUSTOM_DOC("Inserts ',' at the end of the line under the cursor, and moves to the next line."){
    seek_end_of_textual_line(app);
    write_text(app, string_u8_litexpr(","));
    move_down_textual(app);
}

CUSTOM_COMMAND_SIG(mod_write_space)
CUSTOM_DOC("Writes a ' '."){
    write_text(app, string_u8_litexpr(" "));
}

CUSTOM_COMMAND_SIG(mod_write_tab)
CUSTOM_DOC("Writes a tab character."){
    write_text(app, string_u8_litexpr("\t"));
}

CUSTOM_COMMAND_SIG(mod_write_underscore)
CUSTOM_DOC("Writes a '_'."){
    write_text(app, string_u8_litexpr("_"));
}

CUSTOM_COMMAND_SIG(mod_write_newline)
CUSTOM_DOC("Writes a newline with the same indent level of the previous line."){
    View_ID view = get_active_view(app, Access_ReadVisible);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWriteVisible);
    if (buffer == 0){
        buffer = view_get_buffer(app, view, Access_ReadVisible);
        if (buffer != 0){
            goto_jump_at_cursor(app);
            lock_jump_buffer(app, buffer);
        }
    } else {
        Scratch_Block scratch(app);
        i64 pos = view_get_cursor_pos(app, view);
        i64 line = get_line_number_from_pos(app, buffer, pos);
        Range_i64 range = {
            get_line_start_pos(app, buffer, line),
            get_line_end_pos(app, buffer, line),
        };
        
        String_Const_u8 string = push_buffer_range(app, scratch, buffer, range);
        i64 space_count = string_find_first_non_whitespace(string);
        String_Const_u8 indentation = string_prefix(string, space_count);
        
        History_Group history_group = history_group_begin(app, buffer);
        
        buffer_replace_range(app, buffer, Ii64(pos), string_u8_litexpr("\n"));
        buffer_replace_range(app, buffer, Ii64(pos+1), indentation);
        
        view_set_cursor_and_preferred_x(app, view, seek_pos(pos+1+indentation.size));
        
        history_group_end(history_group);
    }
}

CUSTOM_COMMAND_SIG(mod_increase_indent)
CUSTOM_DOC("Increases the indentation of the line under the cursor."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    i64 pos = view_get_cursor_pos(app, view);
    i64 line_start = get_line_start_pos(app, buffer, get_line_number_from_pos(app, buffer, pos));
    
    u64 space_count = def_get_config_u64(app, vars_save_string_lit("virtual_whitespace_regular_indent"));
    String_Const_u8 spaces = push_string_const_u8(scratch, space_count);
    block_fill_u8(spaces.str, space_count, ' ');
    
    buffer_replace_range(app, buffer, Ii64(line_start), spaces);
}

CUSTOM_COMMAND_SIG(mod_decrease_indent)
CUSTOM_DOC("Decreases the indentation of the line under the cursor."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    i64 pos = view_get_cursor_pos(app, view);
    i64 line_start = get_line_start_pos(app, buffer, get_line_number_from_pos(app, buffer, pos));
    u64 space_count = def_get_config_u64(app, vars_save_string_lit("virtual_whitespace_regular_indent"));
    
    Range_i64 range = Ii64(line_start, line_start + space_count);
    String_Const_u8 string = push_buffer_range(app, scratch, buffer, range);
    if(string_find_first_non_whitespace(string) == string.size) {
        buffer_replace_range(app, buffer, range, string_u8_empty);
    }
}

CUSTOM_COMMAND_SIG(mod_range_increase_indent)
CUSTOM_DOC("Increases the range indentation."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    u64 space_count = def_get_config_u64(app, vars_save_string_lit("virtual_whitespace_regular_indent"));
    String_Const_u8 spaces = push_string_const_u8(scratch, space_count);
    block_fill_u8(spaces.str, space_count, ' ');
    
    Range_i64 range = get_view_range(app, view);
    i64 min_line = get_line_number_from_pos(app, buffer, range.min);
    i64 max_line = get_line_number_from_pos(app, buffer, range.max);
    
    i64 line_count = max_line - min_line + 1;
    
    Batch_Edit *first = 0;
    Batch_Edit *last = 0;
    
    for(i32 i = 0; i < line_count; ++i) {
        Buffer_Seek line_start = seek_line_col(min_line + i, 0);
        Buffer_Cursor cursor = buffer_compute_cursor(app, buffer, line_start);
        
        Batch_Edit *edit = push_array(scratch, Batch_Edit, 1);
        sll_queue_push(first, last, edit);
        edit->edit.text = spaces;
        edit->edit.range = Ii64(cursor.pos);
    }
    
    if(first) buffer_batch_edit(app, buffer, first);
}

CUSTOM_COMMAND_SIG(mod_range_decrease_indent)
CUSTOM_DOC("Decreases the range indentation."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    u64 space_count = def_get_config_u64(app, vars_save_string_lit("virtual_whitespace_regular_indent"));
    
    Range_i64 range = get_view_range(app, view);
    i64 min_line = get_line_number_from_pos(app, buffer, range.min);
    i64 max_line = get_line_number_from_pos(app, buffer, range.max);
    
    i64 line_count = max_line - min_line + 1;
    
    Batch_Edit *first = 0;
    Batch_Edit *last = 0;
    
    for(i32 i = 0; i < line_count; ++i) {
        Buffer_Seek line_start = seek_line_col(min_line + i, 0);
        Buffer_Cursor cursor = buffer_compute_cursor(app, buffer, line_start);
        Range_i64 edit_range = Ii64(cursor.pos, cursor.pos + space_count);
        String_Const_u8 string = push_buffer_range(app, scratch, buffer, edit_range);
        
        if(string_find_first_non_whitespace(string) == string.size) {
            Batch_Edit *edit = push_array(scratch, Batch_Edit, 1);
            sll_queue_push(first, last, edit);
            edit->edit.text = string_u8_empty;
            edit->edit.range = edit_range;
        }
    }
    
    if(first) buffer_batch_edit(app, buffer, first);
}

CUSTOM_COMMAND_SIG(mod_comment_line_toggle)
CUSTOM_DOC("Toggles '// ' at the beggining of the line under the cursor, then move the cursor to the next line."){
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    History_Group history_group = history_group_begin(app, buffer);
    i64 pos = get_start_of_line_at_cursor(app, view, buffer);
    b32 alread_has_comment = c_line_comment_starts_at_position(app, buffer, pos);
    if (alread_has_comment){
        i32 delete_offset = 2;
        if(buffer_get_char(app, buffer, pos + 2) == ' ') delete_offset = 3;
        buffer_replace_range(app, buffer, Ii64(pos, pos + delete_offset), string_u8_empty);
    }
    else{
        buffer_replace_range(app, buffer, Ii64(pos), string_u8_litexpr("// "));
    }
    move_down(app);
    seek_beginning_of_textual_line(app);
    history_group_end(history_group);
}

CUSTOM_COMMAND_SIG(to_camelcase)
CUSTOM_DOC("Converts all ascii text in the range between the cursor and the mark to camelcase."){
    View_ID view = get_active_view(app, Access_ReadWriteVisible);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWriteVisible);
    Range_i64 range = get_view_range(app, view);
    Scratch_Block scratch(app);
    String_Const_u8 string = push_buffer_range(app, scratch, buffer, range);
    for (u64 i = 0; i < string.size; i += 1){
        if((!character_is_alpha(string.str[i-1]) || string.str[i-1] == '_') &&
           character_is_alpha(string.str[i])) {
            string.str[i] = character_to_upper(string.str[i]);
        } else {
            string.str[i] = character_to_lower(string.str[i]);
        }
    }
    buffer_replace_range(app, buffer, range, string);
    view_set_cursor_and_preferred_x(app, view, seek_pos(range.max));
}

CUSTOM_COMMAND_SIG(mod_include_gaurd)
CUSTOM_DOC("Inserts a c-style include gaurd around the current buffer, using the current buffer file name."){
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_ReadWrite);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadWrite);
    
    String_Const_u8 file_name = string_front_of_path(push_buffer_file_name(app, scratch, buffer));
    
    for(i32 i = 0; i < file_name.size; ++i){
        if(character_is_lower(file_name.str[i])){
            file_name.str[i] = character_to_upper(file_name.str[i]);
        }
        else if(!character_is_alpha_numeric(file_name.str[i])){
            file_name.str[i] = '_';
        }
    }
    
    Batch_Edit edits[2] = {};
    edits[0].edit.text = push_u8_stringf(scratch, "#if !defined(_%.*s)\n#define _%.*s\n\n",
                                         string_expand(file_name), string_expand(file_name));
    edits[0].edit.range = Ii64();
    edits[0].next = &edits[1];
    
    i64 buffer_size = buffer_get_size(app, buffer);
    edits[1].edit.text = push_u8_stringf(scratch, "\n\n#endif // _%.*s", string_expand(file_name));
    edits[1].edit.range = Ii64(buffer_size);
    
    buffer_batch_edit(app, buffer, edits);
    view_set_cursor(app, view, seek_line_col(4, 0));
}

CUSTOM_COMMAND_SIG(mod_kill_buffer)
CUSTOM_DOC("Kills the current buffer, or switches to the next buffer if it cannot be killed."){
    View_ID view = get_active_view(app, Access_ReadVisible);
    Buffer_ID buffer = view_get_buffer(app, view, Access_ReadVisible);
    Buffer_Kill_Result result = try_buffer_kill(app, buffer, view, 0);
    if(result == BufferKillResult_Unkillable) {
        
    }
}

CUSTOM_COMMAND_SIG(mod_nop)
CUSTOM_DOC("Does nothing at all."){
    // I am empty on purpose.
}

// Shortcuts commands for vim-like usage

CUSTOM_COMMAND_SIG(q)
CUSTOM_DOC("Attempts to close 4coder."){
    exit_4coder(app);
}

CUSTOM_COMMAND_SIG(w)
CUSTOM_DOC("Saves the current buffer."){
    save(app);
}

CUSTOM_COMMAND_SIG(wq)
CUSTOM_DOC("Saves the current buffer."){
    save(app);
    exit_4coder(app);
}

#endif // _MODAL_COMMANDS_CPP