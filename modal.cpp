#if !defined(_MODAL_CPP)
#define _MODAL_CPP
#define FCODER_DEFAULT_BINDINGS_CPP

#include "4coder_default_include.cpp"

// NOTE(allen): Users can declare their own managed IDs here.
String_ID mapid_vital;
String_ID mapid_common;

// @Modes
String_ID mode_normal;
String_ID mode_insert;
String_ID chord_insert;
String_ID mode_replace;
String_ID chord_replace;
String_ID mode_query;
String_ID mode_search;
String_ID mode_goto;
String_ID chord_case;
String_ID chord_snippet;
String_ID chord_preferences;

#include "modal.h"
#include "modal_variables.h"
#include "modal_theme.cpp"

//
// Modes
//

function void modal_set_theme(Application_Links *app, Managed_ID mapid) {
    modal_set_base_theme(app);
    
    // @Modes
    if     (mapid == mode_normal)       modal_set_theme_mode_normal(app);
    else if(mapid == mode_insert)       modal_set_theme_mode_insert(app);
    else if(mapid == chord_insert)      modal_set_theme_chord_insert(app);
    else if(mapid == mode_replace)      modal_set_theme_mode_replace(app);
    else if(mapid == chord_replace)     modal_set_theme_chord_replace(app);
    // else if(mapid == mode_query)        modal_set_theme_mode_query(app); <-- intentional!
    else if(mapid == mode_search)       modal_set_theme_mode_search(app);
    else if(mapid == mode_goto)         modal_set_theme_mode_goto(app);
    else if(mapid == chord_case)        modal_set_theme_chord_case(app);
    else if(mapid == chord_snippet)     modal_set_theme_chord_snippet(app);
    else if(mapid == chord_preferences) modal_set_theme_chord_preferences(app);
    
    if(modal_inverted_theme_colors) modal_invert_theme_colors();
}

function void modal_set_active_mapid(Application_Links *app, Command_Map_ID mapid) {
    if(global_active_mapid != mapid) {
        global_last_mapid = global_active_mapid;
        global_active_mapid = mapid;
        modal_set_theme(app, mapid);
    }
}

function void modal_set_mode_by_mapid(Application_Links *app, Managed_ID mapid) {
    // @Modes
    if     (mapid == mode_normal)       modal_set_mode_normal(app);
    else if(mapid == mode_insert)       modal_set_mode_insert(app);
    else if(mapid == chord_insert)      modal_set_chord_insert(app);
    else if(mapid == mode_replace)      modal_set_mode_replace(app);
    else if(mapid == chord_replace)     modal_set_chord_replace(app);
    else if(mapid == mode_query)        modal_set_mode_query(app);
    else if(mapid == mode_search)       modal_set_mode_search(app);
    else if(mapid == mode_goto)         modal_set_mode_goto(app);
    else if(mapid == chord_case)        modal_set_chord_case(app);
    else if(mapid == chord_snippet)     modal_set_chord_snippet(app);
    else if(mapid == chord_preferences) modal_set_mode_preferences(app);
}

function b32 modal_mode_should_close_hud(Managed_ID mapid) {
    // @Modes
    if     (mapid == mode_normal)       return true;
    else if(mapid == mode_insert)       return true;
    else if(mapid == chord_insert)      return true;
    else if(mapid == mode_replace)      return true;
    else if(mapid == chord_replace)     return true;
    else if(mapid == mode_query)        return true;
    else if(mapid == mode_search)       return true;
    else if(mapid == mode_goto)         return true;
    else if(mapid == chord_case)        return false;
    else if(mapid == chord_snippet)     return false;
    else if(mapid == chord_preferences) return false;
    
    return true;
}

inline function void modal_revert_mode(Application_Links *app) {
    modal_set_mode_by_mapid(app, global_last_mapid);
}

inline function b32 mod_active_buffer_has_write_access(Application_Links *app) {
    View_ID view = get_active_view(app, Access_Write);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Write);
    b32 result = (buffer_get_access_flags(app, buffer) & Access_Write);
    return result;
}

function b32 modal_mapid_inherits_from(Application_Links *app, Command_Map_ID mapid, Command_Map_ID parent) {
    Mapping *mapping = &framework_mapping;
    Command_Map *map = mapping_get_map(mapping, mapid);
    b32 result = false;
    
    while(map) {
        if(map->id == parent) {
            result = true;
            break;
        } else if (map->id == map->parent) {
            Assert(map->id == (Command_Map_ID)mapid_vital);
        }
        map = mapping_get_map(mapping, map->parent);
    }
    
    return result;
}

function b32 modal_active_mode_inherits_from(Application_Links *app, Command_Map_ID parent) {
    return modal_mapid_inherits_from(app, global_active_mapid, parent);
}

#include "generated/managed_id_metadata.cpp"
#include "modal_draw.cpp"
#include "modal_bindings_hud.cpp"
#include "modal_hooks.cpp"
#if OS_WINDOWS
#include "debugger_remedybg.cpp"
#else
#include "debugger_stub.cpp"
#endif

#include "modal_commands.cpp"

// @Modes
#include "modes/normal.cpp"
#include "modes/insert.cpp"
#include "modes/replace.cpp"
#include "modes/query.cpp"
#include "modes/search.cpp"
#include "modes/goto.cpp"
#include "modes/case.cpp"
#include "modes/snippet.cpp"
#include "modes/preferences.cpp"

#include "modal_keyboard_macro.cpp"
#include "modal_cli_commands.cpp"

function void modal_set_mapping(Mapping *mapping) {
    MappingScope();
    
    SelectMapping(mapping);
    mapid_vital = vars_save_string_lit("mapid_vital");
    SelectMap(mapid_vital);
    {
        BindCore(modal_startup, CoreCode_Startup);
        BindCore(default_try_exit, CoreCode_TryExit);
        BindCore(clipboard_record_clip, CoreCode_NewClipboardContents);
        Bind(command_lister, KeyCode_Semicolon, KeyCode_Control, KeyCode_Shift);
        
        Bind(mod_describe_binding,  KeyCode_H, KeyCode_Shift, KeyCode_Control);
        Bind(mod_show_bindings_hud, KeyCode_ForwardSlash, KeyCode_Shift, KeyCode_Control);
        
        Bind(exit_4coder, KeyCode_F4, KeyCode_Alt);
        Bind(exit_4coder, KeyCode_Q,  KeyCode_Control);
        
        Bind(mod_keyboard_macro_finish_recording, KeyCode_Q, KeyCode_Control, KeyCode_Shift);
        
        Bind(project_fkey_command, KeyCode_F1);
        Bind(project_fkey_command, KeyCode_F2);
        Bind(project_fkey_command, KeyCode_F3);
        Bind(project_fkey_command, KeyCode_F4);
        Bind(project_fkey_command, KeyCode_F5);
        Bind(project_fkey_command, KeyCode_F6);
        Bind(project_fkey_command, KeyCode_F7);
        Bind(project_fkey_command, KeyCode_F8);
        Bind(project_fkey_command, KeyCode_F9);
        Bind(project_fkey_command, KeyCode_F10);
        Bind(project_fkey_command, KeyCode_F11);
        Bind(project_fkey_command, KeyCode_F12);
        Bind(project_fkey_command, KeyCode_F13);
        Bind(project_fkey_command, KeyCode_F14);
        Bind(project_fkey_command, KeyCode_F15);
        Bind(project_fkey_command, KeyCode_F16);
        
        BindMouseWheel(mouse_wheel_scroll);
    }
    
    SelectMapping(mapping);
    mapid_common = vars_save_string_lit("mapid_common");
    SelectMap(mapid_common);
    {
        ParentMap(mapid_vital);
        BindTextInput(write_text_input);
        
        Bind(mod_toggler,       KeyCode_Space, KeyCode_Alt);
        Bind(open_panel_vsplit, KeyCode_Equal, KeyCode_Control);
        Bind(open_panel_hsplit, KeyCode_Minus, KeyCode_Control);
        Bind(close_panel,       KeyCode_Equal, KeyCode_Control, KeyCode_Shift);
        Bind(close_panel,       KeyCode_Minus, KeyCode_Control, KeyCode_Shift);
        
        Bind(move_left,  KeyCode_Left);
        Bind(move_right, KeyCode_Right);
        Bind(move_up,    KeyCode_Up);
        Bind(move_down,  KeyCode_Down);
        
        Bind(mod_move_left_alpha_numeric_or_camel_boundary,  KeyCode_Left,  KeyCode_Control);
        Bind(mod_move_left_alpha_numeric_or_camel_boundary,  KeyCode_H,     KeyCode_Control);
        Bind(mod_move_right_alpha_numeric_or_camel_boundary, KeyCode_Right, KeyCode_Control);
        Bind(mod_move_right_alpha_numeric_or_camel_boundary, KeyCode_L,     KeyCode_Control);
        
        Bind(move_up_to_blank_line,   KeyCode_Up,   KeyCode_Control);
        Bind(move_up_to_blank_line,   KeyCode_K,    KeyCode_Control);
        Bind(move_down_to_blank_line, KeyCode_Down, KeyCode_Control);
        Bind(move_down_to_blank_line, KeyCode_J,    KeyCode_Control);
        
        Bind(move_left_whitespace_boundary,  KeyCode_Left,  KeyCode_Alt);
        Bind(move_left_whitespace_boundary,  KeyCode_H,     KeyCode_Alt);
        Bind(move_right_whitespace_boundary, KeyCode_Right, KeyCode_Alt);
        Bind(move_right_whitespace_boundary, KeyCode_L,     KeyCode_Alt);
        Bind(move_up_10,                     KeyCode_Up,    KeyCode_Alt);
        Bind(move_up_10,                     KeyCode_K,     KeyCode_Alt);
        Bind(move_down_10,                   KeyCode_Down,  KeyCode_Alt);
        Bind(move_down_10,                   KeyCode_J,     KeyCode_Alt);
        
        Bind(move_line_up,   KeyCode_Up,   KeyCode_Control, KeyCode_Shift);
        Bind(move_line_up,   KeyCode_K,    KeyCode_Control, KeyCode_Shift);
        Bind(move_line_down, KeyCode_Down, KeyCode_Control, KeyCode_Shift);
        Bind(move_line_down, KeyCode_J,    KeyCode_Control, KeyCode_Shift);
        
        Bind(delete_char, KeyCode_Delete);
        Bind(delete_char, KeyCode_Delete, KeyCode_Shift);
        Bind(delete_alpha_numeric_boundary, KeyCode_Delete, KeyCode_Control);
        
        Bind(mod_write_newline, KeyCode_Return);
        
        Bind(backspace_char,     KeyCode_Backspace);
        Bind(mod_backspace_word, KeyCode_Backspace, KeyCode_Control);
        Bind(mod_backspace_word, KeyCode_W,         KeyCode_Control);
        Bind(mod_backspace_line, KeyCode_Backspace, KeyCode_Control, KeyCode_Shift);
        Bind(snipe_backward_whitespace_or_token_boundary, KeyCode_Backspace, KeyCode_Alt);
        Bind(snipe_forward_whitespace_or_token_boundary,  KeyCode_E,         KeyCode_Alt);
        
        Bind(seek_beginning_of_textual_line, KeyCode_Home);
        Bind(seek_end_of_textual_line,       KeyCode_End);
        
        Bind(seek_beginning_of_line, KeyCode_Home, KeyCode_Control);
        Bind(seek_end_of_line,       KeyCode_End,  KeyCode_Control);
        
        Bind(goto_beginning_of_file, KeyCode_Home, KeyCode_Alt);
        Bind(goto_end_of_file,       KeyCode_End,  KeyCode_Alt);
        
        Bind(page_up,   KeyCode_PageUp);
        Bind(page_down, KeyCode_PageDown);
        
        Bind(set_mark,         KeyCode_Space,     KeyCode_Control);
        Bind(cursor_mark_swap, KeyCode_Semicolon, KeyCode_Control);
        
        Bind(select_all,   KeyCode_A, KeyCode_Control);
        Bind(delete_range, KeyCode_D, KeyCode_Alt);
        
        Bind(duplicate_line, KeyCode_D, KeyCode_Control);
        
        Bind(copy,           KeyCode_C, KeyCode_Control);
        Bind(mod_copy_token, KeyCode_C, KeyCode_Alt);
        Bind(cut,            KeyCode_X, KeyCode_Control);
        Bind(mod_cut_token,  KeyCode_X, KeyCode_Alt);
        Bind(paste,          KeyCode_V, KeyCode_Control);
        Bind(paste_next,     KeyCode_V, KeyCode_Control, KeyCode_Shift);
        
        Bind(save, KeyCode_S, KeyCode_Control);
        Bind(undo, KeyCode_Z, KeyCode_Control);
        Bind(redo, KeyCode_Z, KeyCode_Control, KeyCode_Shift);
        
        Bind(word_complete,     KeyCode_Tab);
        Bind(mod_write_tab,     KeyCode_Tab, KeyCode_Shift);
        Bind(auto_indent_range, KeyCode_Tab, KeyCode_Control);
        
        Bind(mod_query_replace,            KeyCode_R, KeyCode_Control);
        Bind(mod_query_replace_identifier, KeyCode_R, KeyCode_Control, KeyCode_Shift);
        Bind(mod_replace_in_range,         KeyCode_R, KeyCode_Alt);
        
        Bind(mod_rectangular_replace,     KeyCode_R,     KeyCode_Control, KeyCode_Alt);
        Bind(mod_rectangular_fill_spaces, KeyCode_Space, KeyCode_Control, KeyCode_Alt);
        Bind(mod_rectangular_delete,      KeyCode_D,     KeyCode_Control, KeyCode_Alt);
        
        Bind(mod_rectangular_copy,           KeyCode_C, KeyCode_Control, KeyCode_Alt);
        Bind(mod_rectangular_cut,            KeyCode_X, KeyCode_Control, KeyCode_Alt);
        Bind(mod_rectangular_paste_repeated, KeyCode_V, KeyCode_Control, KeyCode_Alt);
        
        Bind(mod_ocd_equals, KeyCode_A, KeyCode_Alt);
        Bind(mod_ocd_after,  KeyCode_A, KeyCode_Control, KeyCode_Shift);
        Bind(mod_ocd_before, KeyCode_A, KeyCode_Control, KeyCode_Alt);
        
        Bind(mod_write_arrow,              KeyCode_Minus,         KeyCode_Alt);
        Bind(mod_append_comma_to_line,     KeyCode_Comma,         KeyCode_Alt);
        Bind(mod_append_semicolon_to_line, KeyCode_Semicolon,     KeyCode_Alt);
        Bind(mod_append_backslash_to_line, KeyCode_BackwardSlash, KeyCode_Alt);
        
        Bind(open_long_braces,           KeyCode_LeftBracket, KeyCode_Control);
        Bind(open_long_braces_semicolon, KeyCode_LeftBracket, KeyCode_Control, KeyCode_Shift);
        Bind(open_long_braces_break,     KeyCode_LeftBracket, KeyCode_Control, KeyCode_Shift);
        
        Bind(mod_comment_line_toggle, KeyCode_ForwardSlash, KeyCode_Control);
        Bind(if0_off,                 KeyCode_ForwardSlash, KeyCode_Alt);
        
        Bind(load_project,      KeyCode_P, KeyCode_Alt);
        Bind(toggle_fullscreen, KeyCode_F, KeyCode_Control);
        
        Bind(modal_set_chord_snippet, KeyCode_S, KeyCode_Alt);
        
        BindMouseWheel(mouse_wheel_change_face_size, KeyCode_Control);
        BindMouse(click_set_cursor, MouseCode_Left);
        BindMouse(click_set_mark,   MouseCode_Left, KeyCode_Shift);
        BindCore(click_set_cursor,  CoreCode_ClickActivateView);
        BindMouseMove(click_set_cursor_if_lbutton);
    }
    
    // @Modes
    mode_normal = vars_save_string_lit("Normal Mode");
    modal_bind_mode_normal(mapping);
    
    mode_insert = vars_save_string_lit("Insert Mode");
    modal_bind_mode_insert(mapping);
    
    chord_insert = vars_save_string_lit("Insert Chord");
    modal_bind_chord_insert(mapping);
    
    mode_replace = vars_save_string_lit("Replace Mode");
    modal_bind_mode_replace(mapping);
    
    chord_replace = vars_save_string_lit("Replace Chord");
    modal_bind_chord_replace(mapping);
    
    mode_query = vars_save_string_lit("Query Mode");
    modal_bind_mode_query(mapping);
    
    mode_search = vars_save_string_lit("Search Mode");
    modal_bind_mode_search(mapping);
    
    mode_goto = vars_save_string_lit("Goto Mode");
    modal_bind_mode_goto(mapping);
    
    chord_case = vars_save_string_lit("Case Chord");
    modal_bind_chord_case(mapping);
    
    chord_snippet = vars_save_string_lit("Snippet Chord");
    modal_bind_chord_snippet(mapping);
    
    chord_preferences = vars_save_string_lit("Preferences Chord");
    modal_bind_chord_preferences(mapping);
    
    String_ID mapid_file = vars_save_string_lit("keys_file");
    SelectMap(mapid_file);
    ParentMap(mode_normal);
    
    String_ID mapid_code = vars_save_string_lit("keys_code");
    SelectMap(mapid_code);
    ParentMap(mode_normal);
}

void
custom_layer_init(Application_Links *app){
    Thread_Context *tctx = get_thread_context(app);
    
    // NOTE(allen): setup for default framework
    default_framework_init(app);
    
    // NOTE(allen): default hooks and command maps
    implicit_map_function = modal_implicit_map;
    set_all_modal_hooks(app);
    
    mapping_init(tctx, &framework_mapping);
    modal_set_mapping(&framework_mapping);
    modal_set_base_theme(app);
}

#endif //_MODAL_CPP