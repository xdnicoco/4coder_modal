#if !defined(_MODAL_H)
#define _MODAL_H

struct Dll_Node_String_Const_u8 {
    Dll_Node_String_Const_u8 *next;
    Dll_Node_String_Const_u8 *prev;
    String_Const_u8 string;
};

enum Goto_Mode {
    goto_mode_abs,
    goto_mode_add,
    goto_mode_sub,
    goto_mode_cancel,
};

struct Goto_State {
    View_ID view;
    i64 target;
    Buffer_Cursor last_cursor;
    Goto_Mode mode;
};

#define MOD_QUERY_STAGE_CAP 8

struct Query_State {
    View_ID view;
    Buffer_ID buffer;
    i64 stage;
    String_u8 string[MOD_QUERY_STAGE_CAP];
    String_Const_u8 prompt[MOD_QUERY_STAGE_CAP];
    
    Dll_Node_String_Const_u8 *history_first;
    Dll_Node_String_Const_u8 *history;
    
    Custom_Command_Function *apply_hook;
    Custom_Command_Function *update_hook;
    Custom_Command_Function *cancel_hook;
    
    String_Match_Flag match_flags;
    
    Range_i64 selection;
    i64 pre_paste_cursor;
    
    i64 completion_pos;
    i32 paste_index;
    
    b32 applied;
    
    Arena arena;
};

struct Search_State {
    Buffer_Cursor last_cursor;
    Query_State query;
    
    Range_i64 match;
    Scan_Direction direction;
    
    b32 do_highlights;
};

#endif // _MODAL_H