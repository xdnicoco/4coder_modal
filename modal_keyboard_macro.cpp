#if !defined(_MODAL_KEYBOARD_MACRO_CPP)
#define _MODAL_KEYBOARD_MACRO_CPP

function User_Input
get_next_non_modifier_input(Application_Links *app, Event_Property get_properties, Event_Property abort_properties) {
    User_Input result = {};
    
    for (;;){
        User_Input in = get_next_input(app, get_properties, abort_properties);
        if((in.event.key.code == KeyCode_Shift)   ||
           (in.event.key.code == KeyCode_Control) ||
           (in.event.key.code == KeyCode_Alt)     ||
           (in.event.key.code == KeyCode_Command) ||
           (in.event.key.code == KeyCode_CapsLock)) {
            continue;
        }
        result = in;
        break;
    }
    
    return result;
}

CUSTOM_COMMAND_SIG(mod_keyboard_macro_start_recording)
CUSTOM_DOC("Start macro recording, do nothing if macro recording is already started.") {
    if (global_keyboard_macro_is_recording ||
        get_current_input_is_virtual(app)){
        return;
    }
    
    Query_Bar_Group group(app);
    Query_Bar bar = {};
    bar.prompt = string_u8_litexpr("Record a keyboard macro: ");
    
    start_query_bar(app, &bar, 0);
    User_Input in = get_next_non_modifier_input(app, EventProperty_AnyKey, EventProperty_Escape);
    end_query_bar(app, &bar, 0);
    if(in.abort) return;
    
    if(global_keyboard_macro_ranges[in.event.key.code].end > 0) {
        Scratch_Block scratch(app);
        bar.prompt = push_u8_stringf(scratch, "Re-record [%s] (y)es, (N)o", key_code_name[in.event.key.code]);
        
        start_query_bar(app, &bar, 0);
        User_Input response = get_next_non_modifier_input(app, EventProperty_AnyKey, EventProperty_Escape);
        end_query_bar(app, &bar, 0);
        if(response.event.key.code != KeyCode_Y) return;
    }
    
    global_keyboard_macro_is_recording = true;
    global_active_macro_code = in.event.key.code;
    Buffer_ID buffer = get_keyboard_log_buffer(app);
    global_keyboard_macro_range.first = buffer_get_size(app, buffer);
}

CUSTOM_COMMAND_SIG(mod_keyboard_macro_finish_recording)
CUSTOM_DOC("Stop macro recording, do nothing if macro recording is not already started.") {
    if (!global_keyboard_macro_is_recording ||
        get_current_input_is_virtual(app)){
        return;
    }
    
    global_keyboard_macro_is_recording = false;
    
    Buffer_ID buffer = get_keyboard_log_buffer(app);
    i64 end = buffer_get_size(app, buffer);
    Buffer_Cursor cursor = buffer_compute_cursor(app, buffer, seek_pos(end));
    Buffer_Cursor back_cursor = buffer_compute_cursor(app, buffer, seek_line_col(cursor.line - 1, 1));
    global_keyboard_macro_range.one_past_last = back_cursor.pos;
    global_keyboard_macro_ranges[global_active_macro_code] = global_keyboard_macro_range;
}

CUSTOM_COMMAND_SIG(mod_keyboard_macro_replay)
CUSTOM_DOC("Replay a keyboard macro.") {
    Buffer_ID keyboard_buffer = get_keyboard_log_buffer(app);
    Scratch_Block scratch(app);
    
    Query_Bar_Group group(app);
    Query_Bar bar = {};
    List_String_Const_u8 list = {};
    for(i32 i = 0; i < KeyCode_COUNT; ++i) {
        if(global_keyboard_macro_is_recording && i == global_active_macro_code) {
            // Please don't recurse a macro inside itself
        } else if(global_keyboard_macro_ranges[i].one_past_last > 0) {
            string_list_push(scratch, &list, SCu8(key_code_name[i]));
        }
    }
    if(list.total_size > 0) {
        bar.prompt = string_u8_litexpr("Replay macro: ");
        bar.string = string_list_flatten(scratch, list, SCu8(" "), 0, StringFill_NullTerminate);
    } else {
        global_one_time_message = string_u8_litexpr("No macros recorded yet.");
        global_show_one_time_message = true;
        return;
    }
    
    start_query_bar(app, &bar, 0);
    User_Input in = get_next_non_modifier_input(app, EventProperty_AnyKey, EventProperty_Escape);
    end_query_bar(app, &bar, 0);
    
    Key_Code code = in.event.key.code;
    Range_i64 range = global_keyboard_macro_ranges[code];
    
    if(in.abort || range.one_past_last == 0 || get_current_input_is_virtual(app) ||
       (global_keyboard_macro_is_recording && global_active_macro_code == code)) {
        return;
    }
    String_Const_u8 macro = push_buffer_range(app, scratch, keyboard_buffer, range);
    
    if(global_keyboard_macro_is_recording) {
        i64 keyboard_buffer_size = buffer_get_size(app, keyboard_buffer);
        buffer_replace_range(app, keyboard_buffer, Ii64(keyboard_buffer_size), macro);
        keyboard_macro_play(app, macro);
    } else {
        global_active_macro_code = code;
        global_keyboard_macro_range = range;
        keyboard_macro_play(app, macro);
    }
}

CUSTOM_COMMAND_SIG(mod_keyboard_macro_replay_last)
CUSTOM_DOC("Replay the last used macro.") {
    if(get_current_input_is_virtual(app)) return;
    if(global_keyboard_macro_ranges[global_active_macro_code].one_past_last == 0){
        global_one_time_message = string_u8_litexpr("No macros recorded yet.");
        global_show_one_time_message = true;
        return;
    }
    
    keyboard_macro_replay(app);
}

#endif // _MODAL_KEYBOARD_MACRO_CPP