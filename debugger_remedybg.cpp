#if !defined(_DEBUGGER_REMEDYBG_CPP)
#define _DEBUGGER_REMEDYBG_CPP

function void remedybg_command_under_cursor(Application_Links *app, String_Const_u8 command) {
    Scratch_Block scratch(app);
    View_ID view = get_active_view(app, Access_Always);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Read);
    
    String_Const_u8 file_name = push_buffer_file_name(app, scratch, buffer);
    String_Const_u8 path = push_hot_directory(app, scratch);
    
    i64 pos = view_get_cursor_pos(app, view);
    i64 line_number = get_line_number_from_pos(app, buffer, pos);
    
    if(file_name.size > 0 && path.size > 0) {
        String_Const_u8 cmd =
            push_u8_stringf(scratch, "remedybg %.*s %.*s %lld",
                            string_expand(command), string_expand(file_name), line_number);
        
        if (cmd.size > 0){
            exec_system_command(app, 0, buffer_identifier(0), path, cmd, 0);
        }
    }
}

function void remedybg_command(Application_Links *app, String_Const_u8 command) {
    Scratch_Block scratch(app);
    String_Const_u8 path = push_hot_directory(app, scratch);
    if(path.size > 0) {
        String_Const_u8 cmd = push_u8_stringf(scratch, "remedybg %.*s", string_expand(command));
        
        if (cmd.size > 0){
            exec_system_command(app, 0, buffer_identifier(0), path, cmd, 0);
        }
    }
}

CUSTOM_COMMAND_SIG(debugger_jump_to_cursor)
CUSTOM_DOC("Sends a signal to remedybg instance to jump to the file and line under the cursor.") {
    remedybg_command_under_cursor(app, string_u8_litexpr("open-file"));
}

CUSTOM_COMMAND_SIG(debugger_add_breakpoint_at_cursor)
CUSTOM_DOC("Sends a signal to remedybg instance to add a breakpoint at the line under the cursor.") {
    remedybg_command_under_cursor(app, string_u8_litexpr("add-breakpoint-at-file"));
}

CUSTOM_COMMAND_SIG(debugger_remove_breakpoint_at_cursor)
CUSTOM_DOC("Sends a signal to remedybg instance to remove a breakpoint at the line under the cursor.") {
    remedybg_command_under_cursor(app, string_u8_litexpr("remove-breakpoint-at-file"));
}

CUSTOM_COMMAND_SIG(debugger_start_debugging)
CUSTOM_DOC("Sends a signal to remedybg instance to start debugging.") {
    remedybg_command(app, string_u8_litexpr("start-debugging"));
}

CUSTOM_COMMAND_SIG(debugger_stop_debugging)
CUSTOM_DOC("Sends a signal to remedybg instance to stop debugging.") {
    remedybg_command(app, string_u8_litexpr("stop-debugging"));
}

CUSTOM_COMMAND_SIG(debugger_continue_execution)
CUSTOM_DOC("Sends a signal to remedybg instance to continue execution.") {
    remedybg_command(app, string_u8_litexpr("continue-execution"));
}

CUSTOM_COMMAND_SIG(debugger_open_project_session)
CUSTOM_DOC("Starts remedybg with the default session (should be saved as session.rdbg along the project.4coder).") {
    Scratch_Block scratch(app);
    
#if 0    
    if (current_project.loaded) {
        String_Const_u8 remedybg_session_file =
            push_u8_stringf(scratch, "%.*s/session.rdbg", string_expand(current_project.dir));
        if(file_exists_and_is_file(app, remedybg_session_file)) {
            String_Const_u8 cmd = push_u8_stringf(scratch, "remedybg %.*s", string_expand(remedybg_session_file));
            
            if (cmd.size > 0){
                exec_system_command(app, 0, buffer_identifier(0), current_project.dir, cmd, 0);
            }
        }
    }
#endif
}

#endif // _DEBUGGER_REMEDYBG_CPP